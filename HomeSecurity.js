import React from 'react';
import { StyleSheet, Text, View, ScrollView, Switch, RefreshControl, Modal, Image, Button, TouchableOpacity, FlatList, Linking } from 'react-native';
import { Picker } from '@react-native-picker/picker'
import WaitingSpinner from './WaitingSpinner';
const playIcon = require('./assets/images/playIcon.png');
import { Asset } from 'expo-asset';
Asset.loadAsync([playIcon]);
const accessTokens = require('./PenniDinhAccessTokens');
const serverEndpoint = require('./ServerEndpoint');

const picturesPerCameraNums = [];
picturesPerCameraNums.push('2');
picturesPerCameraNums.push('4');
picturesPerCameraNums.push('6');
picturesPerCameraNums.push('10');
picturesPerCameraNums.push('12');
picturesPerCameraNums.push('16');
for (var i = 2; i < 1000; i ++) {
    picturesPerCameraNums.push((i * 10) + '');
}
import { SecurityCameraImages, NumberOfImagesPicker } from './media/Tile'

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

const INTERVAL_MS = (1000 * 60 * 5);

class HourAgoPicker extends React.Component {
    createPickerItem(epochTimeMS) {
        const date = new Date(0);
        date.setUTCSeconds(epochTimeMS / 1000);

        return (<Picker.Item key={epochTimeMS} label={date.toLocaleDateString("en-US", { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' }) + ' ' + formatAMPM(date)} value={epochTimeMS} />);
    }

    render() {
        const pickerItems = [];
        let intervalMS = INTERVAL_MS;
        let iteration = 0;
        let latest = 0;
        for (let i = this.props.initialValue; i < this.props.latestObjectTimeMs; i = i + intervalMS) {
            if (i >= this.props.latestObjectTimeMs) {
                oneMore = false;
            }

            pickerItems.push(this.createPickerItem(i));

            iteration++;
            if (iteration > 24) {
                intervalMS = 1000 * 60 * 60 * 24;
            } else if (iteration > 12) {
                intervalMS = 1000 * 60 * 60;
            }

            latest = i;
        }
        if (latest !== this.props.latestObjectTimeMs) {
            pickerItems.push(this.createPickerItem(this.props.latestObjectTimeMs));
        }
        pickerItems.reverse();

        oneMore = true;
        intervalMS = INTERVAL_MS;
        iteration = 0;
        for (let i = this.props.initialValue; i > this.props.earliestObjectTimeMs; i = i - intervalMS) {
            if (i !== this.props.initialValue) {
                pickerItems.push(this.createPickerItem(i));
            }

            iteration++;
            if (iteration > 24) {
                intervalMS = 1000 * 60 * 60 * 24;
            } else if (iteration > 12) {
                intervalMS = 1000 * 60 * 60;
            }

            latest = i;
        }
        if (latest !== this.props.earliestObjectTimeMs && this.props.earliestObjectTimeMs !== this.props.latestObjectTimeMs) {
            pickerItems.push(this.createPickerItem(this.props.earliestObjectTimeMs));
        }
        pickerItems.reverse();


        return (<Picker
            selectedValue={this.props.initialValue}
            onValueChange={(itemValue) =>
                this.props.onChange(itemValue)
            }
            style={{width: '100%', borderTopWidth: 1, borderBottomWidth: 1, borderColor: 'grey'}}>
            {pickerItems}
        </Picker>)
    }
}

class CameraPicker extends React.Component {
    render() {
        var pickers = this.props.cameraNames.map(cameraName => {
            return (<Picker.Item key={cameraName} label={cameraName.replace(/-/g, ' ').replace(/\b\w/g, l => l.toUpperCase())} value={cameraName} />);
        });

        return (<Picker
                selectedValue={this.props.initialValue}
                onValueChange={(itemValue) =>
                this.props.setCamera(itemValue) }
                style={{width: '50%', height: 100}} itemStyle={{height: 100}}
                >
                 <Picker.Item label="all (merged)" value="all-merged" />
                {pickers}
                </Picker>)
    }
}

export default class HomeSecurity extends React.Component {
    async componentDidMount() {
        return this.handleRefresh();
    }

    handleRefresh = () => {
        this.setState({refreshing: true});
        var that = this;
        accessTokens.get((accessToken) => {
          serverEndpoint.get(function(serverEndpoint, proxyPort) {
              let headers = new Headers();
              headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
              fetch("https://" + serverEndpoint + "/secure-node-js/securityFileCameraNames.json", {headers: headers})
                .then(res => res.json())
                  .then((res) => {
                    const cameraNames = res.cameraNames;

                    const imagesPerCamera = this.state && this.state.imagesPerCamera ? this.state.imagesPerCamera : 12;
                    const pageNum = this.state && this.state.pageNum ? this.state.pageNum : 1;
                    const offset = imagesPerCamera * (pageNum - 1);
                    const camerasToShow = this.state && this.state.cameraToShow && this.state.cameraToShow !== 'all-merged' ? this.state.cameraToShow : null;
                    const camerasToShowQueryParam = camerasToShow !== null ? '&dirNames=' + camerasToShow : '';
                    const mediaTypesQueryParam = (this.state && this.state.mediaTypes != null) ? '&mediaTypes=' + Array.from(this.state.mediaTypes).join(',') : '&mediaTypes=video';
                    const url = "https://" + serverEndpoint + "/secure-node-js/securityFilesV2.json?limit=" + imagesPerCamera + "&offset=" + offset + camerasToShowQueryParam + mediaTypesQueryParam;
                    fetch(url, {headers: headers})
                      .then(res => res.json())
                                   .then((res) => {
                                         var securityFileObjects = res
                                          .files
                                          .map(val => {
                                             const urnPath = (val.filePath.startsWith('/home/pi/ftp') ? '/home-security' + val.filePath.substring('/home/pi/ftp'.length) : val.filePath).replaceAll(' ', '%20');
                                             const homeSecurityPath = val.filePath.startsWith('/home/pi/ftp') ? val.filePath.substring('/home/pi/ftp'.length) : val.filePath;

                                             return {urnPath: urnPath, homeSecurityPath: homeSecurityPath, creationTimeMs: val.creationTimeMs, mediaType: val.mediaType, cameraName: val.name};
                                         })
                                         .filter(item => item != null);

                                         that.setState({totalCount:res.count, refreshing: false, cameras: cameraNames, securityFileObjects: securityFileObjects});
                                         })
                      .catch(err => console.log(err));
                  })
                .catch(err => console.log(err));
          }.bind(this));
        });
    };

    toggleFilterOpen = () => {
        this.setState({filterOpen: !(this.state.filterOpen === true)});
    };

    removeMediaType = (mediaType) => {
       const mediaTypes = this.state && this.state.mediaTypes ? this.state.mediaTypes : new Set(['video']);

       mediaTypes.delete(mediaType);

       this.setState({mediaTypes: mediaTypes});
       this.handleRefresh();
    }

    addMediaType = (mediaType) => {
       let mediaTypes = (this.state && this.state.mediaTypes) ? this.state.mediaTypes : new Set(['video']);

       mediaTypes.add(mediaType);

       this.setState({mediaTypes: mediaTypes});
       this.handleRefresh();
    }

    render() {
        if (this.state == null || this.state.securityFileObjects == null) {
         return (<WaitingSpinner />);
        } else if (this.state != null && this.state.securityFileObjects != null) {
            var cameraToShow = this.state.cameraToShow != null ? this.state.cameraToShow : 'all-merged';

            var homeSecurityFileObjectsSorted = this.state.securityFileObjects
                .sort((a, b) => {
               return b.creationTimeMs - a.creationTimeMs;
           });

            var pageNum = this.state.pageNum != null ? this.state.pageNum : 1;
            const imagesPerCamera = this.state.imagesPerCamera != null ? this.state.imagesPerCamera : 12;
            const totalPages = Math.ceil(this.state.totalCount / imagesPerCamera);

           const securityImageComponents = (<SecurityCameraImages openWebview={this.props.openWebview} earliestSecurityFileEpochTime={homeSecurityFileObjectsSorted.length > 0 ? homeSecurityFileObjectsSorted[homeSecurityFileObjectsSorted.length - 1].creationTimeMs / 1000 : 0} latestSecurityFileEpochTime={homeSecurityFileObjectsSorted.length > 0 ? homeSecurityFileObjectsSorted[0].creationTimeMs / 1000 : 0} update={this.handleRefresh} canSave={true} totPages={totalPages} pageNum={pageNum} key={cameraToShow} homeSecurityFileObjectsFilteredAndSortedAndPagedForCamera={homeSecurityFileObjectsSorted} prettyName={cameraToShow === 'all-merged' ? 'All Cameras - Timeline' : (cameraToShow.replace(/-/g, ' ').replace(/\b\w/g, l => l.toUpperCase()))} />);

           var maxNumPages = [];
           for(var i = 0; i < totalPages; i++){
               maxNumPages.push(i + 1);
           }

           return (<ScrollView style={{paddingTop: 30}} refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.handleRefresh}/>}>
               <View  style={{backgroundColor: '#6cd938', width: '30%', left: '60%', marginTop: 20, borderRadius: 10}}><Button color='black' title={'Show Filter'} onPress={this.toggleFilterOpen}/></View>
               <Modal style={{}} transparent={false} animationType='fade' visible={this.state.filterOpen === true} onRequestClose={this.toggleFilterOpen}>
                   <View  style={{backgroundColor: '#6cd938', width: '30%', left: '10%', marginTop: 50, borderRadius: 10}}><Button color='black' title={'Hide Filter'} onPress={this.toggleFilterOpen}/></View>
                   <View>
                       <Text style={{paddingLeft: 20, paddingTop: 2}}>Include Image Files?</Text>
                       <Switch  style={{marginLeft: 20, paddingTop: 1}}
                                value={this.state.mediaTypes && this.state.mediaTypes.has('image')}
                                onValueChange={(value => { value ? this.addMediaType('image') : this.removeMediaType('image')  })}/>
                       <Text style={{paddingLeft: 20, paddingTop: 2}}>Include Video Files?</Text>
                       <Switch  style={{marginLeft: 20, paddingTop: 1}}
                                value={!this.state.mediaTypes || this.state.mediaTypes.has('video')}
                                onValueChange={(value => { value ? this.addMediaType('video') : this.removeMediaType('video') })}/>
                   </View>
               </Modal>
               <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                   <NumberOfImagesPicker labelPrefix={'Page '} labelSuffix={''} numbers={maxNumPages} setNumber={(newNumber) => { this.setState({pageNum: newNumber}); this.handleRefresh(); }} initialValue={pageNum + ''} />
                   <NumberOfImagesPicker labelPrefix={''} labelSuffix={' files'} numbers={picturesPerCameraNums} setNumber={(newNumber) => { this.setState({imagesPerCamera: newNumber}); this.handleRefresh(); } } initialValue={imagesPerCamera + ''} />
                   <CameraPicker setCamera={(newCamera) => { this.setState({cameraToShow: newCamera}); this.handleRefresh();} } initialValue={cameraToShow} cameraNames={this.state.cameras} />
               </View>
               {securityImageComponents}
               <View style={{height: 75}} />
           </ScrollView>);
        }
    }
}

const styles = StyleSheet.create({
});
