import React from "react";
import {
    Text, View, Clipboard, ScrollView
} from 'react-native';
import WaitingSpinner from "./WaitingSpinner";
const accessTokens = require('./PenniDinhAccessTokens');
const serverEndpoint = require('./ServerEndpoint');

export default class InfoComponent extends React.Component {
    componentDidMount() {
        accessTokens.get(function(accessToken) {
            serverEndpoint.get(function(serverEndpoint, proxyPort) {
                let headers = new Headers();
                headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
                fetch("https://" + serverEndpoint + "/secure-node-js/hostInfo.js", {headers: headers})
                    .then(res => res.json())
                    .then((res) => {
                        this.setState({hostInfo: res});
                    })
                    .catch(err => console.log(err));
                fetch("https://" + serverEndpoint + "/secure-node-js/fileSystemStats.json", {headers: headers})
                    .then(res => res.json())
                    .then((res) => {
                        this.setState({fileSystemStats: res});
                    })
                    .catch(err => console.log(err));
                fetch("https://" + serverEndpoint + "/secure-node-js/pendingVideoThumbnails.json", {headers: headers})
                    .then(res => res.json())
                    .then((res) => {
                        this.setState({pendingVideoThumbnailPaths: res});
                    })
                    .catch(err => console.log(err));
            }.bind(this));
        }.bind(this));
    }

    saveToClipboardFunction = (string) => {
        return function() {
            Clipboard.setString(string);
            alert(string + ' saved to your clipboard!');
        }.bind(this);
    }

    render() {
        if (this.state && this.state.hostInfo) {
            let hostInfo;
            if (this.state.hostInfo) {
                hostInfo = <View>
                        <Text style={{fontSize: 25, marginTop: 25}}>External PenniDinh Subdomain</Text>
                        <Text onPress={this.saveToClipboardFunction(this.state.hostInfo['domainName'])} style={{marginTop: 20, fontSize: 20}}>Subdoman:</Text>
                        <Text onPress={this.saveToClipboardFunction(this.state.hostInfo['domainName'])} >{this.state.hostInfo['domainName']} (touch to copy to clipboard)</Text>
                        <Text onPress={this.saveToClipboardFunction(this.state.hostInfo['externalIPAddress'])}  style={{marginTop: 20, fontSize: 20}}>IP Address:</Text>
                        <Text onPress={this.saveToClipboardFunction(this.state.hostInfo['externalIPAddress'])} >{this.state.hostInfo['externalIPAddress']}  (touch to copy to clipboard)</Text>
                        <Text style={{fontSize: 25, marginTop: 25}}>Internal PenniDinh Subdomain</Text>
                        <Text onPress={this.saveToClipboardFunction('internal-' + this.state.hostInfo['domainName'])}  style={{marginTop: 20, fontSize: 20}}>Subdoman:</Text>
                        <Text onPress={this.saveToClipboardFunction('internal-' + this.state.hostInfo['domainName'])} >internal-{this.state.hostInfo['domainName']} (touch to copy to clipboard)</Text>
                        <Text onPress={this.saveToClipboardFunction(this.state.hostInfo['internalIPAddress'])}  style={{marginTop: 20, fontSize: 20}}>IP Address:</Text>
                        <Text onPress={this.saveToClipboardFunction(this.state.hostInfo['internalIPAddress'])} >{this.state.hostInfo['internalIPAddress']}  (touch to copy to clipboard)</Text>
                </View>
            } else {
                hostInfo = null;
            }

            let fileSystemStats;
            if (this.state.fileSystemStats) {
                const availOutOfAllRatio = (this.state.fileSystemStats['free'] / this.state.fileSystemStats['total']);
                const usedPercentageString = ((1 - availOutOfAllRatio) * 100).toFixed(2).toString() + '%';
                const availPercentageString = ((availOutOfAllRatio) * 100).toFixed(2).toString() + '%';

                fileSystemStats = <View>
                    <Text style={{fontSize: 25}}>Disk</Text>
                    <Text style={{marginTop: 20, fontSize: 20}}>Total Space: {(this.state.fileSystemStats['total'] / 1024 / 1024 / 1024).toFixed(2)} GB</Text>
                    <Text style={{marginTop: 10, fontSize: 20}}>Available: {availPercentageString}</Text>
                    <View style={{flexDirection: 'row', marginTop: 2}}><View style={{backgroundColor: 'green', height: 10, width: availPercentageString}} /><View style={{backgroundColor: '#DCDCDC', height: 10, width: usedPercentageString}} /></View>
                </View>
            } else {
                fileSystemStats = null;
            }

            let pendingVideoThumbnails;
            if (this.state.pendingVideoThumbnailPaths) {
                const paths = [];
                for (let path of this.state.pendingVideoThumbnailPaths) {
                    paths.push(<Text key={path} style={{marginTop: 2, fontSize: 12}}>{path}</Text>)
                }

                pendingVideoThumbnails = <View>
                    <Text style={{fontSize: 25, marginTop: 25}}>Thumbnail Generator</Text>
                    <Text style={{marginTop: 20, fontSize: 20}}>Pending: {this.state.pendingVideoThumbnailPaths.length ? this.state.pendingVideoThumbnailPaths.length : 'none - all caught up!'}</Text>

                    <View>
                        {paths}
                    </View>
                </View>
            } else {
                pendingVideoThumbnails = null;
            }

            return <ScrollView style={{top: 50, left: '10%', width: '80%'}}>
                {fileSystemStats}
                {hostInfo}
                {pendingVideoThumbnails}
                <View style={{height: 150}} />
            </ScrollView>
        } else {
            return <View style={{height: '100%', width: '100%'}}><WaitingSpinner /></View>;
        }
    }
}
