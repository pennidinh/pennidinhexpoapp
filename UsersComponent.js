import React from "react";

import {
    Text, View, Switch, Button, TextInput, Alert, Clipboard, ScrollView
} from 'react-native';
import WaitingSpinner from "./WaitingSpinner";
const accessTokens = require('./PenniDinhAccessTokens');
const serverEndpoint = require('./ServerEndpoint');
const safeJsonStringify = require('safe-json-stringify');

export default class UsersComponent extends React.Component {
    componentDidMount() {
        this.reloadUsers();
    }

    reloadUsers = () => {
        accessTokens.get(function(accessToken) {
            serverEndpoint.get(function(serverEndpoint, proxyPort) {
                let headers = new Headers();
                headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
                fetch("https://" + serverEndpoint + "/secure-node-js/getAdminUsers.json", {headers: headers})
                    .then(res => res.json())
                    .then((allowedUsers) => {
                        this.setState({allowedUsers: allowedUsers});
                    })
                    .catch(err => alert('error fetching allowed email address users' + err));

                fetch("https://" + serverEndpoint + "/secure-node-js/userInfo.json", {headers: headers})
                    .then(res => res.json())
                    .then((userInfo) => {
                        this.setState({userEmail: userInfo['email']});
                    })
                    .catch(err => alert('Error fetching user email address: ' + err));
            }.bind(this));
        }.bind(this));

    }

    submitPressed = () => {
        if (!this.state.inputEmailAddress) {
            this.setState({errorText: 'Invalid: email address blank'});
            return;
        }

        if (this.state.inputEmailAddress.match(/\s/)) {
            this.setState({errorText: 'Invalid: email address contains spaces'});
            return;
        }

        if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.state.inputEmailAddress)) {
            this.setState({errorText: 'Invalid: email address not valid'});
            return;
        }

        if (this.state.allowedUsers.includes(this.state.inputEmailAddress)) {
            this.setState({errorText: 'Email address already in allow list'});
            return;
        }

        this.setState({disableSubmit: true, errorText: null});
        accessTokens.get(function(accessToken) {
            serverEndpoint.get(function(serverEndpoint, proxyPort) {
                let headers = new Headers();
                headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
                fetch("https://" + serverEndpoint + "/secure-node-js/addAdminUsers.js?users=" + safeJsonStringify([this.state.inputEmailAddress]), {headers: headers, method: 'post'})
                    .then(res => res.json())
                    .then((ftpUsers) => {
                        this.setState({disableSubmit: false, inputEmailAddress: null});
                        this.reloadUsers();
                    })
                    .catch((err) => {
                        alert('Error adding user email');
                        this.setState({disableSubmit: false});
                        reloadUsers();
                    });
            }.bind(this));
        }.bind(this));
    };

    inputEmailAddressChange = (newEmailAddress) => {
        this.setState({inputEmailAddress: newEmailAddress});
    };

    addSelf = () => {
        this.setState({errorText: null});
        accessTokens.get(function(accessToken) {
            serverEndpoint.get(function(serverEndpoint, proxyPort) {
                let headers = new Headers();
                headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
                fetch("https://" + serverEndpoint + "/secure-node-js/addAdminUsers.js?users=" + safeJsonStringify([this.state.userEmail]), {headers: headers, method: 'post'})
                    .then(res => res.json())
                    .then((ftpUsers) => {
                        this.setState({disableSubmit: false, inputEmailAddress: null});
                        this.reloadUsers();
                    })
                    .catch((err) => {
                        alert('Error removing users');
                        this.setState({});
                        reloadUsers();
                    });
            }.bind(this));
        }.bind(this));
    };

    removeUserFunction = (userEmail) => {
        return function() {
            Alert.alert('Remove user email', 'Are you sure you\'d like to remove this user email address?', [{
               text: 'Yes - remove ' + userEmail, onPress: () => {
                   accessTokens.get(function(accessToken) {
                       serverEndpoint.get(function(serverEndpoint, proxyPort) {
                           let headers = new Headers();
                           headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
                           fetch("https://" + serverEndpoint + "/secure-node-js/removeAdminUsers.js?users=" + safeJsonStringify([userEmail]), {headers: headers, method: 'post'})
                               .then(res => res.json())
                               .then((ftpUsers) => {
                                   this.setState({disableSubmit: false, inputEmailAddress: null});
                                   this.reloadUsers();
                               })
                               .catch((err) => {
                                   alert('Error removing users');
                                   this.setState({});
                                   reloadUsers();
                               });
                       }.bind(this));
                   }.bind(this));
                }}, {text: 'No'}], {cancelable: true});
        }.bind(this);
    };

    removeAll = () => {
       Alert.alert('Remove all users', 'Are you sure you\'d like to remove all users and open up your PenniDinh to everyone?', [{
          text: 'Yes - open up to everyone', onPress: () => {
              this.setState({errorText: null});
              accessTokens.get(function(accessToken) {
                  serverEndpoint.get(function(serverEndpoint, proxyPort) {
                      let headers = new Headers();
                      headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
                      fetch("https://" + serverEndpoint + "/secure-node-js/removeAdminUsers.js?users=" + safeJsonStringify(this.state.allowedUsers), {headers: headers, method: 'post'})
                          .then(res => res.json())
                          .then((ftpUsers) => {
                              this.setState({disableSubmit: false, inputEmailAddress: null});
                              this.reloadUsers();
                          })
                          .catch((err) => {
                              alert('Error removing users');
                              this.setState({});
                              reloadUsers();
                          });
                  }.bind(this));
              }.bind(this));
          }
      }, {text: 'No (highly recommended)'}], {cancelable: true});
    };

    render() {
        if (this.state && this.state.allowedUsers !== undefined && this.state.allowedUsers !== null) {
            let body;
            if (this.state.allowedUsers.length > 0) {
                const allowedUsersRender = this.state.allowedUsers.map(function(allowedUser) { return <View style={{flexDirection: 'row'}} key={allowedUser}><Text onPress={this.removeUserFunction(allowedUser)} style={{fontSize: 20, color: 'blue'}} >{ allowedUser != this.state.userEmail ? '[X]' : '' }</Text><Text style={{fontSize: 15}} key={allowedUser}>{allowedUser}</Text></View> }.bind(this));

                body = <View>
                        <Text style={{paddingTop: 20, fontSize: 20}}>Enable email-based allow list of users to your PenniDinh (highly recommended)</Text>
                        <Switch onValueChange={this.removeAll} value={true} />
                        <Text style={{paddingTop: 20, fontSize: 20}}>Allowed User Email Addresses:</Text>
                        {allowedUsersRender}
                        <Text style={{paddingTop: 20, fontSize: 20}}>Enter New Email Address:</Text>
                        <TextInput value={this.state.inputEmailAddress} onChangeText={this.inputEmailAddressChange} autoCorrect={false} style={{textAlign: 'center', backgroundColor: 'white', marginTop: 5, fontSize: 18, borderColor: '#CCCCCC', borderTopWidth: 1, borderBottomWidth: 1, width: '80%', left: '10%'}} />
                        <Text style={{paddingLeft: 10, paddingRight: 10, textAlign: 'center', paddingTop: 2, fontSize: 15, color: 'red', fontWeight: 'bold'}}>{this.state && this.state.errorText ? this.state.errorText : ' '}</Text>
                        <View style={{marginTop: 20, backgroundColor: '#6cd938', width: '80%', left: '10%', borderRadius: 10}}><Button disable={this.state && this.state.disableSubmit} color='black' title={this.state && this.state.disableSubmit ? 'Submitting...' : 'Submit'} onPress={this.submitPressed}/></View>
                    </View>
            } else if (this.state.userEmail) {
                body = <View>
                         <Text style={{paddingTop: 20, fontSize: 20, color: 'red'}}>Enable email-based allow list of users to your PenniDinh (highly recommended)</Text>
                         <Switch onValueChange={this.addSelf} value={false} />
                       </View>
            }

            return <ScrollView style={{top: 50, left: '10%', width: '80%'}}>
                        {body}
                        <View style={{height: 150}} />
                    </ScrollView>
        } else {
            return <View style={{height: '100%', width: '100%'}}><WaitingSpinner /></View>;
        }
    }
}
