import React from "react";

import {
    Text, View, Button, Switch, TextInput, Alert, Clipboard, ScrollView
} from 'react-native';
import WaitingSpinner from "./WaitingSpinner";
const accessTokens = require('./PenniDinhAccessTokens');
const serverEndpoint = require('./ServerEndpoint');
const safeJsonStringify = require('safe-json-stringify');

export default class FtpComponent extends React.Component {
    componentDidMount() {
        accessTokens.get(function(accessToken) {
            serverEndpoint.get(function(serverEndpoint, proxyPort) {
                let headers = new Headers();
                headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
                fetch("https://" + serverEndpoint + "/secure-node-js/ftpUsers.json", {headers: headers})
                    .then(res => res.json())
                    .then((ftpUsers) => {
                        this.setState({ftpUser: ftpUsers[0], savedPassword: ftpUsers[0].password});
                    })
                    .catch(err => alert('error fetching ftp user info' + err));

                fetch("https://" + serverEndpoint + "/secure-node-js/hostInfo.js", {headers: headers})
                    .then(res => res.json())
                    .then((res) => {
                        this.setState({hostInfo: res});
                    })
                    .catch(err => console.log(err));
            }.bind(this));
        }.bind(this));
    }

    submitPressed = () => {
        if (!this.state.ftpUser.password) {
            this.setState({errorText: 'Invalid: password blank'});
            return;
        }

        if (this.state.ftpUser.password.match(/\s/)) {
            this.setState({errorText: 'Invalid: password contains spaces'});
            return;
        }

        if (this.state.ftpUser.password.length >= 255) {
            this.setState({errorText: 'Invalid: password greater than 254 characters'});
            return;
        }

        this.setState({disableSubmit: true, errorText: null});
        accessTokens.get(function(accessToken) {
            serverEndpoint.get(function(serverEndpoint, proxyPort) {
                let headers = new Headers();
                headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
                fetch("https://" + serverEndpoint + "/secure-node-js/setFtpUser.js?username=" + this.state.ftpUser.username + '&password=' + this.state.ftpUser.password, {headers: headers, method: 'post'})
                    .then(res => res.json())
                    .then((ftpUsers) => {
                        this.setState({ftpUser: ftpUsers[0], savedPassword: ftpUsers[0].password, disableSubmit: false, displayedPassword: null});
                        alert('Password updated successfully!');
                    })
                    .catch((err) => {
                        alert('error updating ftp user');
                        this.setState({disableSubmit: false});
                    });
            }.bind(this));
        }.bind(this));
    };

    passwordChange = (password) => {
        const ftpUser = this.state.ftpUser;
        ftpUser['password'] = password;
        this.setState({ftpUser: ftpUser, displayedPassword: password});
    };

    ftpEnabledUpdated = (updatedValue) => {
        if (updatedValue) {
            accessTokens.get(function(accessToken) {
                serverEndpoint.get(function(serverEndpoint, proxyPort) {
                    let headers = new Headers();
                    headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);

                    fetch("https://" + serverEndpoint + "/secure-node-js/setFtpUser.js?username=" + this.state.ftpUser.username + '&password=ChangeMe', {headers: headers, method: 'post'})
                        .then(res => {
                          return res.json();
                         })
                        .then((ftpUsers) => {
                            this.setState({ftpUser: ftpUsers[0], savedPassword: ftpUsers[0].password});
                        })
                        .catch(err => {
                          alert('error creating ftp user' + err);
                        });
                }.bind(this));
            }.bind(this));
        } else {
            Alert.alert('Disable FTP and delete FTP username and password', "Are you sure you'd like to disable FTP? This will delete your FTP username and password. Your files uploaded over FTP will not be deleted.", [{
                text: 'Yes, disable FTP',
                onPress: () => {
                    accessTokens.get(function(accessToken) {
                        serverEndpoint.get(function(serverEndpoint, proxyPort) {
                            let headers = new Headers();
                            headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
                            fetch("https://" + serverEndpoint + "/secure-node-js/setFtpUser.js?username=" + this.state.ftpUser.username + '&password=', {headers: headers, method: 'post'})
                                .then(res => res.json())
                                .then((ftpUsers) => {
                                    this.setState({ftpUser: ftpUsers[0], savedPassword: ftpUsers[0].password});
                                })
                                .catch(err => alert('error disabling ftp' + err));
                        }.bind(this));
                    }.bind(this));
                }
            }, {
                text: 'No', onPress: () => { }
            }, {text: 'Cancel'}], {cancelable: true});
        }
    };

    saveToClipboardFunction = (string) => {
        return function() {
            Clipboard.setString(string);
            alert(string + ' saved to your clipboard!');
        }.bind(this);
    }

    render() {
        if (this.state && this.state.ftpUser) {
            let body;
            if (this.state.savedPassword) {
                let ipAddress;
                if (this.state.hostInfo && this.state.hostInfo['internalIPAddress']) {
                    ipAddress = <View>
                        <Text onPress={this.saveToClipboardFunction(this.state.hostInfo['internalIPAddress'])} style={{textAlign: 'center', paddingTop: 20, fontSize: 20}}>(Internal) IP Address</Text>
                        <Text onPress={this.saveToClipboardFunction(this.state.hostInfo['internalIPAddress'])} style={{textAlign: 'center', paddingTop: 2, fontSize: 18}}>{this.state.hostInfo['internalIPAddress']} (touch to save to clipboard)</Text>
                    </View>
                } else {
                    ipAddress = null;
                }

                body = <View>
                    {ipAddress}
                    <Text onPress={this.saveToClipboardFunction('21')} style={{textAlign: 'center', paddingTop: 20, fontSize: 20}}>Port</Text>
                    <Text onPress={this.saveToClipboardFunction('21')} style={{textAlign: 'center', paddingTop: 2, fontSize: 18}}>21 (touch to save to clipboard)</Text>
                    <Text onPress={this.saveToClipboardFunction(this.state.ftpUser['username'])} style={{textAlign: 'center', paddingTop: 20, fontSize: 20}}>Username</Text>
                    <Text onPress={this.saveToClipboardFunction(this.state.ftpUser['username'])} style={{textAlign: 'center', paddingTop: 2, fontSize: 18}}>{this.state.ftpUser['username']} (touch to save to clipboard)</Text>
                    <Text onPress={this.saveToClipboardFunction(this.state.savedPassword)} style={{textAlign: 'center', paddingTop: 20, fontSize: 20}}>Password</Text>
                    <Text onPress={this.saveToClipboardFunction(this.state.savedPassword)} style={{textAlign: 'center', paddingTop: 2, fontSize: 18}}>{this.state.savedPassword} (touch to save to clipboard)</Text>
                    <TextInput value={this.state.displayedPassword} onChangeText={this.passwordChange} autoCorrect={false} style={{textAlign: 'center', backgroundColor: 'white', marginTop: 5, fontSize: 18, borderColor: '#CCCCCC', borderTopWidth: 1, borderBottomWidth: 1, width: '80%', left: '10%'}} />
                    <Text style={{paddingLeft: 10, paddingRight: 10, textAlign: 'center', paddingTop: 2, fontSize: 15, color: 'red', fontWeight: 'bold'}}>{this.state && this.state.errorText ? this.state.errorText : ' '}</Text>
                    <View style={{marginTop: 20, backgroundColor: '#6cd938', width: '80%', left: '10%', borderRadius: 10}}><Button disable={this.state && this.state.disableSubmit} color='black' title={this.state && this.state.disableSubmit ? 'Submitting...' : 'Submit'} onPress={this.submitPressed}/></View>
                </View>
            } else {
                body = null;
            }

            return <ScrollView style={{top: 50, left: '10%', width: '80%'}}>
                        <Text style={{fontSize: 20}}>FTP Enabled:</Text>
                        <Switch onValueChange={this.ftpEnabledUpdated} value={!!this.state.savedPassword} />
                        {body}
                        <View style={{height: 150}} />
                    </ScrollView>
        } else {
            return <View style={{height: '100%', width: '100%'}}><WaitingSpinner /></View>;
        }
    }
}
