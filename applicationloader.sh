#!/bin/bash
APPFILE=$1
set -euo pipefail

KEY="37Q2XNQR2T"
ISSUER="0cbc32d7-489c-495e-9c16-e5bb468839fe"
xcrun altool --upload-app --type ios --file $APPFILE --apiKey $KEY --apiIssuer $ISSUER
