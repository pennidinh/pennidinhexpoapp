import React from "react";
import {
    Text, View, TextInput, Button
} from 'react-native';
const uuidv1 = require('uuid/v1');

export default class AddEndpointComponent extends React.Component {
    submitPressed = () => {
        if (!this.state || (!this.state.name && !this.state.subdomain)) {
            this.setState({errorText: 'Invalid: name and subdomain blank'});
            return;
        }

        if (!this.state.name) {
            this.setState({errorText: 'Invalid: name blank'});
            return;
        }

        if (!this.state.subdomain) {
            this.setState({errorText: 'Invalid: subdomain blank'});

            return;
        }

        if (this.state.subdomain.indexOf(' ') >= 0) {
            this.setState({errorText: 'Invalid: subdomain contains spaces'});
            return;
        }

        if (!/^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}$/.test(this.state.subdomain)) {
            this.setState({errorText: 'Invalid: subdomain may contain letters, numbers, and dashes'});
            return;
        }

        const fullDomain = this.state.subdomain.toLowerCase() + '.pennidinh.com';
        this.setState({disableSubmit: true, errorText: null});
        fetch('https://subdomainlookup.api.pennidinh.com/exists?subdomainToLookup=' + fullDomain)
            .then(function (response) {

                this.setState({disableSubmit: false});

                if (response['status'] === 400) {
                    this.setState({errorText: fullDomain + ' is not registered with PenniDinh'});
                } else if (response['status'] === 200) {
                    const fullDomainWithoutInternal = fullDomain.indexOf('internal-') >= 0 ? fullDomain.substring('internal-'.length, fullDomain.length) : fullDomain;

                    this.props.submitNewHub(this.state.name, fullDomainWithoutInternal, uuidv1());
                } else {
                    this.setState({errorText: 'Server Unavailable'});
                }
            }.bind(this))
            .catch(function () {
                this.setState({disableSubmit: false, errorText: 'Server Unavailable'});
            });

    };

    nameChanged = (name) => {
        this.setState({name: name});
    };

    subdomainChanged = (subdomain) => {
        this.setState({subdomain: subdomain});
    };

    render() {
        return <View>
            <View style={{marginTop: 45, backgroundColor: '#6cd938', width: '80%', left: '10%', borderRadius: 10}}><Button color='black' title={'Go Back'} onPress={this.props.goBack}/></View>
            <Text style={{textAlign: 'center', paddingTop: 45, fontSize: 25}}>Add an Endpoint</Text>
            <Text style={{textAlign: 'center', paddingTop: 20, fontSize: 20}}>Name</Text>
            <TextInput onChangeText={this.nameChanged} autoCorrect={false} style={{textAlign: 'center', backgroundColor: 'white', marginTop: 5, fontSize: 20, borderColor: '#CCCCCC', borderTopWidth: 1, borderBottomWidth: 1, width: '80%', left: '10%'}} placeholder={'ex. Lake House'} />
            <Text style={{textAlign: 'center', paddingTop: 45, fontSize: 20}}>PenniDinh Subdomain</Text>
            <TextInput onChangeText={this.subdomainChanged} autoCorrect={false} style={{textAlign: 'center', backgroundColor: 'white', marginTop: 5, fontSize: 20, borderColor: '#CCCCCC', borderTopWidth: 1, borderBottomWidth: 1, width: '80%', left: '10%'}} placeholder={'ex. penniman-brooklynn'} />
            <Text style={{textAlign: 'center', paddingTop: 5, fontSize: 20}}>.pennidinh.com</Text>
            <Text style={{paddingLeft: 10, paddingRight: 10, textAlign: 'center', paddingTop: 2, fontSize: 15, color: 'red', fontWeight: 'bold'}}>{this.state && this.state.errorText ? this.state.errorText : ' '}</Text>
            <View style={{marginTop: 20, backgroundColor: '#6cd938', width: '80%', left: '10%', borderRadius: 10}}><Button disable={this.state && this.state.disableSubmit} color='black' title={this.state && this.state.disableSubmit ? 'Submitting...' : 'Submit'} onPress={this.submitPressed}/></View>
        </View>;
    }
}
