import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image, Switch, RefreshControl, Button, TouchableOpacity, WebView, FlatList } from 'react-native';
import Slider from 'react-native-slider';
import TimerMixin from 'react-timer-mixin';
const uuidv1 = require('uuid/v1');
const safeJsonStringify = require('safe-json-stringify');
const base64 = require('base-64');
import WaitingSpinner from './WaitingSpinner';
const accessTokens = require('./PenniDinhAccessTokens');
const serverEndpoint = require('./ServerEndpoint');
const sliderImage = require('./assets/images/Pennidinh_App_Logo_v01_smaller.png');
import { Asset } from 'expo-asset';
Asset.loadAsync([sliderImage]);

class TempRange extends React.Component {
    setPower = (newValue) => {
        var that = this;
        accessTokens.get((accessToken) => {
             serverEndpoint.get(function(serverEndpoint, proxyPort) {
                 let headers = new Headers();
                 headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
                 fetch("https://" + serverEndpoint + "/secure-node-js/setHomeHeater.json?powerSwitchIPAddress=" + that.props.powerSwitchIPAddress + "&powerSwitchIPPort=" + that.props.powerSwitchIPPort + '&thermosatDeviceID=' + that.props.thermosatDeviceID + "&newHighRangeTempFer=100&newCurrentPower=" + newValue, {headers: headers})
                 .then(res => setTimeout(that.props.homeHeaters.handleRefresh, 100))
                 .catch(err => alert(err));
             });
        });
    }
    
    updateTemp = (newValue) => {
        var that = this;
        accessTokens.get((accessToken) => {
          serverEndpoint.get(function(serverEndpoint, proxyPort) {
              let headers = new Headers();
              headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
              fetch("https://" + serverEndpoint + "/secure-node-js/setHomeHeater.json?powerSwitchIPAddress=" + that.props.powerSwitchIPAddress + "&powerSwitchIPPort=" + that.props.powerSwitchIPPort + '&thermosatDeviceID=' + that.props.thermosatDeviceID + "&newHighRangeTempFer=100&newLowRangeTempFer=" + newValue, {headers: headers})
                .then(res => setTimeout(that.props.homeHeaters.handleRefresh, 100))
                .catch(err => alert(err));
          });
        });
    }
    
    render() {
        return (<View style={{}}>
                <Switch
                value={this.props.currentPower}
                onValueChange={(value => this.setPower(value))}/>
                <Slider
                disabled={!this.props.currentPower}
                minimumValue={40}
                maximumValue={90}
                value={parseFloat(this.props.lowRangeTemp)}
                thumbImage={sliderImage}
                thumbStyle={{width: 0, height: 0, left: -16, top: 5}}
                onSlidingComplete={(value) => this.updateTemp(value)} />
                <Text style={{fontSize: 20}}>Target Temp: {this.props.lowRangeTemp}°F</Text>
                <Text style={{fontSize: 20}}>Current Temp: {this.props.currentTemp}°F</Text>
                </View>);
    }
}

class HomeHeater extends React.Component {
    render() {
        return (<View style={{paddingBottom: 20}}>
                <Text style={{fontSize: 30}}>{this.props.name}</Text>
                <TempRange lowRangeTemp={this.props.lowRangeTemp} highRangeTemp={this.props.highRangeTemp} powerSwitchIPAddress={this.props.powerSwitchIPAddress} thermosatDeviceID={this.props.thermosatDeviceID} homeHeaters={this.props.homeHeaters} powerSwitchIPPort={this.props.powerSwitchIPPort}
                    currentTemp={this.props.currentTemp}
                    currentPower={this.props.currentPower}/>
               </View>);
    }
}

export default class HomeHeaters extends React.Component {
    async componentDidMount() {
        return this.handleRefresh();
    }
    
    setHomeHeaters = (homeHeaters) => {
        this.setState({homeHeaters: homeHeaters})
    }
    
    handleRefresh = () => {
        var that = this;
        accessTokens.get((accessToken) => {
          serverEndpoint.get(function(serverEndpoint, proxyPort) {
              let headers = new Headers();
              headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
              fetch("https://" + serverEndpoint + "/secure-node-js/getHomeHeaters.json", {headers: headers})
                             .then(res => res.json())
                .then(res => that.setHomeHeaters(res))
                .catch(err => alert('lose'));
          });
        });
    }
    
    render() {
        if (this.state == null || this.state.homeHeaters == null) {
            return (<WaitingSpinner />);
        } else if (this.state != null && this.state.homeHeaters != null) {
            var heaterRenderedElements = [];
            for (var heaterIndex in this.state.homeHeaters) {
                var heater = this.state.homeHeaters[heaterIndex];
                
                heaterRenderedElements.push(<HomeHeater lowRangeTemp={heater.currentLowRangeTempFer}
                    lowRangeTemp={heater.currentLowRangeTempFer}
                    currentPower={heater.power}
                    currentTemp={heater.currentTempFer}
                    homeHeaters={this}
                    powerSwitchIPPort={heater.powerSwitchIPPort}
                    powerSwitchIPAddress={heater.powerSwitchIPAddress}
                    name={heater.name}
                    thermosatDeviceID={heater.thermosatDeviceID}/>);
            }
            
            return (<ScrollView style={{paddingTop: 30}}>
                    {heaterRenderedElements}
                    </ScrollView>);
        }
    }
}

const styles = StyleSheet.create({
});
