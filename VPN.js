import React from "react";

import {
    Text, View, Switch, Button, TextInput, Alert, Clipboard, ScrollView, Linking
} from 'react-native';
import WaitingSpinner from "./WaitingSpinner";
const accessTokens = require('./PenniDinhAccessTokens');
const serverEndpoint = require('./ServerEndpoint');
const safeJsonStringify = require('safe-json-stringify');

const ALLOWED_CLIENT_NAME_AND_PASSWORD = /^[a-zA-Z0-9\-]*$/;

export default class VPNComponent extends React.Component {
    componentDidMount() {
        this.reloadUsers();
    }

    componentWillUnmount() {
      if (this.state && this.state.interval) {
        clearInterval(this.state.interval);
        this.setState({interval: null});
      }
    }

    reloadUsers = () => {
        accessTokens.get(function(accessToken) {
            serverEndpoint.get(function(serverEndpoint, proxyPort) {
                let headers = new Headers();
                headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);

                fetch("https://" + serverEndpoint + "/secure-node-js/pendingNewVPNClients.js", {headers: headers})
                  .then(res => res.json())
                  .then(function( pendingNewClients ) {
                    this.setState({pendingNewClients: pendingNewClients.map((clientObj) => JSON.parse(clientObj)['name'])})

                    if (pendingNewClients.length > 0) {
                      if (!this.state || !this.state.interval) {
                        const interval = setInterval(this.reloadUsers, 1000);
                        this.setState({interval: interval})
                      }
                    }
                  }.bind(this))
                  .catch(err => alert('error fetching pending new VPN clients' + err));
                fetch("https://" + serverEndpoint + "/secure-node-js/pendingRemoveVPNClients.js", {headers: headers})
                  .then(res => res.json())
                  .then(function( pendingRemoveClients ) {
                    this.setState({pendingRemoveClients: pendingRemoveClients});

                    if (pendingRemoveClients.length > 0) {
                      if (!this.state || !this.state.interval) {
                        const interval = setInterval(this.reloadUsers, 1000);
                        this.setState({interval: interval})
                      }
                    }
                  }.bind(this))
                  .catch(err => alert('error fetching pending deleted VPN clients' + err));
                fetch("https://" + serverEndpoint + "/secure-node-js/existingVPNClients.js", {headers: headers})
                  .then(res => res.json())
                  .then(function( existingClients ) {
                    this.setState({existingClients: existingClients})
                  }.bind(this))
                  .catch(err => alert('error fetching existing VPN clients' + err));
            }.bind(this));
        }.bind(this));

    }

    submitPressed = () => {
        if (!this.state.inputClientName) {
            this.setState({errorText: 'Invalid: client name is blank'});
            return;
        }
        if (this.state.inputClientName.match(/\s/)) {
            this.setState({errorText: 'Invalid: client name contains spaces'});
            return;
        }
        if (!ALLOWED_CLIENT_NAME_AND_PASSWORD.test(this.state.inputClientName)) {
            this.setState({errorText: 'Invalid: email address not valid'});
            return;
        }
        if (this.state.existingClients.includes(this.state.inputClientName)) {
            this.setState({errorText: 'Invalid: Client name contains invalid characters'});
            return;
        }

        if (this.state.inputPassword != this.state.inputPasswordCopy) {
          this.setState({errorText: 'Invalid: passwords do not match'});
          return;
        }

        if (!this.state.inputPassword) {
          this.setState({errorText: 'Invalid: password is blank'});
          return;
        }
        if (this.state.inputPassword.match(/\s/)) {
            this.setState({errorText: 'Invalid: password contains spaces'});
            return;
        }
        if (!ALLOWED_CLIENT_NAME_AND_PASSWORD.test(this.state.inputPassword)) {
            this.setState({errorText: 'Invalid: password contains invalid characters'});
            return;
        }

        this.setState({disableSubmit: true, errorText: null});
        accessTokens.get(function(accessToken) {
            serverEndpoint.get(function(serverEndpoint, proxyPort) {
                let headers = new Headers();
                headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
                fetch("https://" + serverEndpoint + "/secure-node-js/addVPNClient.js?name=" + encodeURIComponent(this.state.inputClientName) + '&password=' + encodeURIComponent(this.state.inputPassword), {headers: headers, method: 'post'})
                    .then((data) => {
                        this.setState({disableSubmit: false, inputClientName: null, inputPassword: null, inputPasswordCopy: null});
                        this.reloadUsers();
                    })
                    .catch((err) => {
                        alert('Error submitting client create request ' + err);
                        this.setState({disableSubmit: false});
                        this.reloadUsers();
                    });
            }.bind(this));
        }.bind(this));
    };

    removeClientFunction = (name) => {
        return function() {
            Alert.alert('Remove VPN Client', 'Are you sure you\'d like to remove this VPN client?', [{
               text: 'Yes - remove ' + name, onPress: () => {
                   accessTokens.get(function(accessToken) {
                       serverEndpoint.get(function(serverEndpoint, proxyPort) {
                           let headers = new Headers();
                           headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
                           fetch("https://" + serverEndpoint + "/secure-node-js/removeVPNClient.js?name=" + encodeURIComponent(name), {headers: headers, method: 'post'})
                               .then(() => {
                                   this.setState({disableSubmit: false});
                                   this.reloadUsers();
                               })
                               .catch((err) => {
                                   alert('Error submitting request to delete VPN client');
                                   this.reloadUsers();
                               });
                       }.bind(this));
                   }.bind(this));
                }}, {text: 'No'}], {cancelable: true});
        }.bind(this);
    };

    downloadVPNClient = (name) => {
      return function() {
        accessTokens.get(function(accessToken) {
            serverEndpoint.get(function(serverEndpoint, proxyPort) {
                if (proxyPort) {
                  Linking.openURL('https://' + serverEndpoint + '/cgi-bin/setProxyEndpoint.cgi?expiringToken=' + encodeURIComponent(accessToken) + '&pennidinhProxyPort=' + proxyPort + '&redirectTo=' + encodeURIComponent('/secure/openvpn/' + name + '.ovpn'));
                } else {
                  Linking.openURL('https://' + serverEndpoint + '/secure-node-js/setLoginCookieAndRedirect.js?accessToken=' + encodeURIComponent(accessToken) + '&redirect_to=' + encodeURIComponent('https://' + serverEndpoint + '/secure/openvpn/' + name + '.ovpn'));
                }
            }.bind(this));
        }.bind(this));
      }.bind(this);
    }

    render() {
        if (this.state && this.state.existingClients !== undefined && this.state.existingClients !== null) {
            let body;
            if (this.state.existingClients.length > 0) {
                const existingUsersRender = this.state.existingClients.map(function(existingClient) {
                  return <View style={{flexDirection: 'row'}} key={existingClient}>
                      <Text onPress={this.removeClientFunction(existingClient)} style={{fontSize: 20, color: 'red'}} >[X]</Text>
                      <Text style={{fontSize: 15}} key={existingClient}>{existingClient}</Text>
                      <Text onPress={this.downloadVPNClient(existingClient)} style={{fontSize: 10, color: 'blue'}} >Download</Text>
                    </View>
                }.bind(this));
                let pendingRemoveClientsRender;
                if (this.state.pendingRemoveClients && this.state.pendingRemoveClients.length > 0) {
                  pendingRemoveClientsRender = (<View>
                        <Text style={{paddingTop: 20, fontSize: 20}}>Delete in progress:</Text>
                        {this.state.pendingRemoveClients.map(function(client) {
                          return <View style={{flexDirection: 'row'}} key={client}>
                              <Text style={{fontSize: 15, color: 'grey'}} key={client}>{client}</Text>
                            </View>
                        }.bind(this))}
                    </View>);
                  } else {
                    pendingRemoveClientsRender = null;
                  }

                  let pendingCreateClientsRender;
                  if (this.state.pendingNewClients && this.state.pendingNewClients.length > 0) {
                    pendingCreateClientsRender = (<View>
                          <Text style={{paddingTop: 20, fontSize: 20}}>Create in progress:</Text>
                          {this.state.pendingNewClients.map(function(client) {
                            return <View style={{flexDirection: 'row'}} key={client}>
                                <Text style={{fontSize: 15, color: 'green'}} key={client}>{client}</Text>
                              </View>
                          }.bind(this))}
                      </View>);
                    } else {
                      pendingCreateClientsRender = null;
                    }

                body = <View>
                        <Text style={{paddingTop: 10, paddingBottom: 10}}>
                          The PenniDinh VPN runs on port 9000 of your PenniDinh Hub. The VPN client configurations below assume your PenniDinh hub is reachable through through public IP address on port 9000; for the VPN feature of PenniDinh to work, your router needs to be configured to forward incoming internet traffic to port 9000 to your PenniDinh hub's port 9000.
                        </Text>
                        <Text style={{paddingTop: 10, paddingBottom: 10}}>
                          OpenVPN <Text style={{color: 'blue', fontWeight: 'bold'}} onPress={() => Linking.openURL('https://itunes.apple.com/us/app/openvpn-connect/id590379981?mt=8')}>iOS</Text> and <Text style={{color: 'blue', fontWeight: 'bold'}} onPress={() => Linking.openURL('https://apkpure.com/openvpn-connect-%E2%80%93-fast-safe-ssl-vpn-client/net.openvpn.openvpn')}>Android</Text> app download
                        </Text>
                        <Text style={{paddingTop: 10, paddingBottom: 10}}>
                        To use your PenniDinh VPN from your phone, install the OpenVPN iOS or Android app. Once installed, opening below VPN client "download" links should download and open up the VPN client in your OpenVPN app.
                        </Text>
                        <Text style={{paddingTop: 20, fontSize: 20}}>Existing VPN Clients:</Text>
                        {existingUsersRender}
                        {pendingRemoveClientsRender}
                        {pendingCreateClientsRender}
                        <Text style={{paddingTop: 20, fontSize: 20}}>Create new VPN Client:</Text>
                        <Text style={{paddingTop: 5, fontSize: 15}}>Client Name</Text>
                        <TextInput value={this.state.inputClientName} onChangeText={(newVal) => this.setState({inputClientName: newVal})} autoCorrect={false} style={{textAlign: 'center', backgroundColor: 'white', marginTop: 5, fontSize: 18, borderColor: '#CCCCCC', borderTopWidth: 1, borderBottomWidth: 1, width: '80%', left: '10%'}} />
                        <Text style={{paddingTop: 5, fontSize: 15}}>Password</Text>
                        <TextInput value={this.state.inputPassword} onChangeText={(newVal) => this.setState({inputPassword: newVal})} autoCorrect={false} style={{textAlign: 'center', backgroundColor: 'white', marginTop: 5, fontSize: 18, borderColor: '#CCCCCC', borderTopWidth: 1, borderBottomWidth: 1, width: '80%', left: '10%'}} />
                        <Text style={{paddingTop: 5, fontSize: 15}}>Password (again)</Text>
                        <TextInput value={this.state.inputPasswordCopy} onChangeText={(newVal) => this.setState({inputPasswordCopy: newVal})} autoCorrect={false} style={{textAlign: 'center', backgroundColor: 'white', marginTop: 5, fontSize: 18, borderColor: '#CCCCCC', borderTopWidth: 1, borderBottomWidth: 1, width: '80%', left: '10%'}} />
                        <Text style={{paddingLeft: 10, paddingRight: 10, textAlign: 'center', paddingTop: 2, fontSize: 15, color: 'red', fontWeight: 'bold'}}>{this.state && this.state.errorText ? this.state.errorText : ' '}</Text>
                        <View style={{marginTop: 20, backgroundColor: '#6cd938', width: '80%', left: '10%', borderRadius: 10}}><Button disable={this.state && this.state.disableSubmit} color='black' title={this.state && this.state.disableSubmit ? 'Submitting...' : 'Submit'} onPress={this.submitPressed}/></View>
                    </View>
            }

            return <ScrollView style={{top: 50, left: '10%', width: '80%'}}>
                        {body}
                        <View style={{height: 150}} />
                    </ScrollView>
        } else {
            return <View style={{height: '100%', width: '100%'}}><WaitingSpinner /></View>;
        }
    }
}
