### Develop

```
npx expo start --tunnel
```

### Expo SDK Bump

Expo releases frequently and only supports for 6 months old version 6 months after the newer one is released.

#### NPM package bump
Bump expo npm version (example to bump to version 498)
```
npm install expo@"28"
```

#### Sync all other expo dependences
```
npx expo install --check
```

### Release

#### Version Bump
Bump the "version" property in app.json (i.e. 0.21.0 to 0.22.0)

#### Build

Run this command which then gives you a download link. Download the .ipa file.

```
eas build -p ios
```

#### Upload

Open "Transporter" MacOS app. Drag the .ipa file to this app.
