import React from 'react';
import { AppRegistry } from "react-native";
import {
    StyleSheet, Text, View, ScrollView, Image, Switch, RefreshControl, Button, TouchableOpacity, ImageBackground, Alert,
    AsyncStorage, Linking, Modal
} from 'react-native';
import { WebView } from 'react-native-webview';
const safeJsonStringify = require('safe-json-stringify');
import Lights from './Lights';
import HomeSecurity from './HomeSecurity';
import HomeClimate from './HomeClimate';
import HomeAlarms from './HomeAlarms';
import Ftp from './FtpComponent';
import VPN from './VPN';
import Users from './UsersComponent';
import FileUploads from './FileUploads';
import HomePlugs from './HomePlugs';
import HomeHeaters from './HomeHeaters';
import AddEndpointComponent from './AddEndpointComponent';
import InfoComponent from './InfoComponent';
const cameraIcon = require('./assets/images/cameraIcon.png');
const cameraSetupIcon = require('./assets/images/Icon-Camera-Setup.png');
const filesIcon = require('./assets/images/Icon-Files.png');
const infoIcon = require('./assets/images/Icon-Hub-Info.png');
const usersIcon = require('./assets/images/Icon-Users.png');
const vpnIcon = require('./assets/images/Icon-VPN.png');
const dashboardBackground = require('./assets/images/Dashboard.png');
const bottomNavBackground = require('./assets/images/bottomNavBackground.png');
const bottomNavLeftIcon = require('./assets/images/bottomNavLeftIcon.png');
const bottomNavRightIcon = require('./assets/images/bottomNavRightIcon.png');
const dottedIcon = require('./assets/images/dottedIcon.png');
import * as Updates from 'expo-updates';
import { Asset } from 'expo-asset';
import WaitingSpinner from "./WaitingSpinner";
Asset.loadAsync([cameraIcon, dottedIcon, dashboardBackground, bottomNavBackground, bottomNavLeftIcon, bottomNavRightIcon, cameraSetupIcon, filesIcon, infoIcon, usersIcon, vpnIcon]);
import * as SecureStore from 'expo-secure-store';
import * as LocalAuthentication from 'expo-local-authentication';

const accessTokens = require('./PenniDinhAccessTokens');
const serverEndpointt = require('./ServerEndpoint');

Updates.checkForUpdateAsync().then( (update) => {
  if (update.isAvailable) {
    Updates.fetchUpdateAsync().then( () => {
      alert('A new update of PenniDinh has been downloaded and will be applied when the app is restarted');
      Updates.reloadAsync();
    }).catch( (err) => {
      console.log(err);
    });
  }
}).catch( (err) => {
  console.log(err);
});

const HUBS_KEY = "hubs-key--";

var cachedName = '';
var cachedEmail = '';
class Name extends React.Component {
    componentDidMount() {
        accessTokens.get((accessToken) => {
                         serverEndpointt.get(function(serverEndpoint, proxyPort) {
                            let headers = new Headers();
                            headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
                            fetch("https://" + serverEndpoint + "/secure-node-js/userInfo.json", {headers: headers})
                            .then(res => res.json())
                            .then((res) => {
                                  var name = res.name;
                                  var email = res.email;
                                  cachedName = name;
                                  cachedEmail = email;
                                  this.setState({refreshing: false, name: name, email: email});
                                  })
                            .catch(err => console.log(err));
                         }.bind(this));
        });
    }

    render () {
        if (this.state != null && this.state.name != null && this.state.email) {
            return (<Text>{this.state.name} ({this.state.email})</Text>);
        } else if (cachedName !== '' && cachedEmail !== '') {
            return (<Text>{cachedName} ({cachedEmail})</Text>);
        } else {
            return (<Text></Text>);
        }
    }
}

class AppBorder extends React.Component {
    render() {
        const bottomButtons = [];
        if (this.props.homeButtonPressed) {
            bottomButtons.push(<TouchableOpacity key={'homeButton'} onPress={this.props.homeButtonPressed} style={{height: 59, width: 100, left: 10, top: 0, position: 'absolute'}}><Image source={bottomNavLeftIcon} style={{height: 25, width: 22, left: 30, top: 8, position: 'absolute'}} onPress={this.props.homeButtonPressed} /></TouchableOpacity>);
        }

        if (this.props.hamburgerButtonPressed) {
            bottomButtons.push(<TouchableOpacity key={'hamburgerButton'} onPress={this.props.hamburgerButtonPressed} style={{height: 59, width: 100, right: 10, top: 0, position: 'absolute'}}><Image source={bottomNavRightIcon} style={{height: 15, width: 25, right: 30, top: 13, position: 'absolute'}} onPress={this.props.hamburgerButtonPressed} /></TouchableOpacity>);
        }

        return (<ImageBackground source={bottomNavBackground} style={{resizeMode: 'stretch', position: 'absolute', bottom: 0, width: '100%', height: 43}}>
                    {bottomButtons}
                </ImageBackground>)
    }
}

class NavBubbleElement extends React.Component {
    render() {
        if (this.props.imageSource != null) {
            return (<View style={{width: '50%', aspectRatio: 1}}><TouchableOpacity onPress={this.props.pressAction} style={{ left: '10%', width: '80%', aspectRatio: 1}}><Image source={this.props.imageSource} style={{flex:1 , width: undefined, height: undefined}} /></TouchableOpacity></View>);
        } else if (this.props.text != null) {
            return (<View style={{width: '50%', aspectRatio: 1}}><TouchableOpacity onPress={this.props.pressAction} style={{left: '10%', width: '80%', aspectRatio: 1}}><ImageBackground source={dottedIcon} style={{aspectRatio: 1, width: '100%'}}><Text style={{top: '50%', left: '45%'}}>{this.props.text}</Text></ImageBackground></TouchableOpacity></View>);
        } else {
            throw new Error("Need to have imageURL or text defined!");
        }
    }
}

const lastLoginTime = {'time': 0};

const LAST_LOGIN_KEY = "lastLoginKey0"

export default class App extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        serverEndpointt.registerEndpointInUseListener('homepage-title', function() {
            this.setState({endpointInUsePrettyName: serverEndpointt.getEndpointInUsePrettyName()});
        }.bind(this));
        accessTokens.registerCurrentProviderListener('modal-logout', function (provider) {
            this.setState({currentProvider: provider});
        }.bind(this));

        SecureStore.getItemAsync(HUBS_KEY).then(function(results) {
            if (!results) {
                return;
            }
            const hubs = JSON.parse(results);

            this.setState({hubs: hubs});

            SecureStore.getItemAsync(LAST_LOGIN_KEY).then(function(results) {
                if (!results) {
                    return;
                }

	        const lastLoginInfo = JSON.parse(results);

		let currentHub = null;
		for (var hubIndex in hubs) {
	          const hub = hubs[hubIndex];
		  if (hub['id'] === lastLoginInfo['hubId']) {
	              currentHub = hub;
	              break;
		  }
		}

		if (!currentHub) {
                    console.log('Last login hub no longer exists');
                    return;
		}

		this.chooseEndpoint(currentHub, function() {
		    this.deviceAuthThenLogin(lastLoginInfo['provider']);
		}.bind(this))();
  	    }.bind(this));
        }.bind(this));
    }

    openWebview = (openWebview) => {
        this.setState({openWebview: openWebview});
    }

    login = (provider) => {
        const selectedPenniDinhHubExternalHostName = this.state.selectedPenniDinhHubExternalHostName;
        const hubUnavailableCallback = function() {
            if (selectedPenniDinhHubExternalHostName === this.state.selectedPenniDinhHubExternalHostName) {
                alert(this.state.selectedPenniDinhHubExternalHostName + ' is offline');
                this.logout();
            }
        }.bind(this);

        this.setState({isAuthenticatingWithSelectedHub: true});
        accessTokens.setProvider(function() {
            accessTokens.get(function(accessToken) {
                if (accessToken === undefined || accessToken == null) {
                    console.log('User not already logged in to hub: ' + selectedPenniDinhHubExternalHostName);
                    this.setState({isAuthenticatingWithSelectedHub: false});
                    return;
                }
                this.setState({hasAccessTokenFromSelectedPenniDinhHub: true, isAuthenticatingWithSelectedHub: false});

                serverEndpointt.get(function(serverEndpoint, proxyPort) {
                  let headers = new Headers();
                  headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
                  fetch("https://" + serverEndpoint + "/secure-node-js/authentication-ping.json", {headers: headers})
                      .then((res) => {
                          if (selectedPenniDinhHubExternalHostName === this.state.selectedPenniDinhHubExternalHostName) {
                            if (res['status'] !== 200) {
                              alert('There was an issue with the request. Please try again.');
                              console.error(safeJsonStringify(res));
                              if (res['status'] >= 400 && res['status'] < 500) {
                                this.logoutForgetKeys();
                              } else {
                                this.logout();
                              }
                              return;
                            }
                          }

                          fetch("https://" + serverEndpoint + "/secure-node-js/authorization-ping.json", {headers: headers})
                            .then((res) => {
                                if (selectedPenniDinhHubExternalHostName === this.state.selectedPenniDinhHubExternalHostName) {
                                    if (res['status'] === 200) {
                                        this.setState({isAuthorizedForSelectedHub: true});

                                	const lastLoginInfo = JSON.stringify({provider: provider, hubId: this.state.currentHubId});
                                        SecureStore.setItemAsync(LAST_LOGIN_KEY, lastLoginInfo).then(function(err) {
                                            if (err) {
						console.log('error setting last login info: ' + err);
                                                return;
                                            }
                                	}.bind(this));
                                    } else if (res['status'] === 401) {
                                        alert('You are not authorized to access: ' + this.state.selectedPenniDinhHubExternalHostName);
                                        this.logoutForgetKeys();
                                    } else {
                                        alert('Error while authorizing. Please try again.');
                                        this.logout();
                                    }
                                }
                            })
                            .catch(err => {
                                alert('Error while authorizing. Please try again.');
                                console.log(err);
                            });
                      })
                      .catch(err => {
                          alert('There was an issue with the request. Please try again.');
                          console.log(err);
                      });
                }.bind(this));
            }.bind(this), hubUnavailableCallback, true);
        }.bind(this), provider, this.state.selectedPenniDinhHubExternalHostName);
    }

    deviceAuthThenLogin = (provider) => {
        LocalAuthentication.hasHardwareAsync()
            .then(function(hasHardwareAuth) {
                if (hasHardwareAuth && !__DEV__ && (new Date() - lastLoginTime['time'] > 1000 * 60 * 10)) { // 10 minutes
                    LocalAuthentication.authenticateAsync('Touch ID required to proceed')
                        .then(function (fulfilled) {
                            if (fulfilled.error) {
                                console.log(fulfilled.error);
                                return;
                            }

                            lastLoginTime['time'] = new Date();

                            if (fulfilled.success) {
                                this.login(provider);
                            }
                        }.bind(this))
                        .catch(function (error) {
                            console.log(error);
                        });

                } else {
                    this.login(provider);
                }
            }.bind(this));
    }

    loginWithAmazon = () => {
        this.deviceAuthThenLogin('amazon');
    }


    loginWithGoogle = () => {
        this.deviceAuthThenLogin('google');
    }


    loginWithFacebook = () => {
        this.deviceAuthThenLogin('facebook');
    }

    resetEndpoint = () => {
        SecureStore.deleteItemAsync('internalEndpoint').then( function() {
              SecureStore.deleteItemAsync('externalEndpoint').then( function() {
                  serverEndpointt.setInternalEndpoint(null);
                  serverEndpointt.setExternalEndpoint(null);
                  serverEndpointt.clearInUseEndpoint();

                  this.setState({selectedPenniDinhHubExternalHostName: null, endpointInUsePrettyName: null});
              }.bind(this));
        }.bind(this));
    }

    logout = (successCallback) => {
        this.setState({currentHubId: null, currentPage: 'home', hasAccessTokenFromSelectedPenniDinhHub: false, sideMenuOpen: false, isAuthorizedForSelectedHub: false, selectedPenniDinhHubExternalHostName: null, endpointInUsePrettyName: null, isAuthenticatingWithSelectedHub: false});
        serverEndpointt.clearInUseEndpoint();
        SecureStore.deleteItemAsync(LAST_LOGIN_KEY).then(function() {
	    if (successCallback) { successCallback() }
	});
    }

    logoutForgetKeys = () => {
	this.logout(function() {
	    accessTokens.logout();
	}.bind(this));
    }

    switchHubs = () => {
        SecureStore.deleteItemAsync('internalEndpoint').then( function() {
            SecureStore.deleteItemAsync('externalEndpoint').then( function() {
                serverEndpointt.setInternalEndpoint(null);
                serverEndpointt.setExternalEndpoint(null);
                serverEndpointt.clearInUseEndpoint();

                accessTokens.switchHubs(function () {
                    this.setState({
                        currentPage: 'home',
                        hasAccessTokenFromSelectedPenniDinhHub: false,
                        selectedPenniDinhHubExternalHostName: null,
                        sideMenuOpen: false,
                        isAuthorizedForSelectedHub: false,
                        endpointInUsePrettyName: null,
                        isAuthenticatingWithSelectedHub : false,
                        currentHubId: null
                    });
                }.bind(this));
            }.bind(this));
        }.bind(this));
    }

    chooseEndpoint = (hub, successCallback) => {
        const internal = 'internal-' + hub['domain'];
        const external = hub['domain'];
        return function() {
            SecureStore.setItemAsync('internalEndpoint', internal).then(function () {
                SecureStore.setItemAsync('externalEndpoint', external).then(function () {
                    serverEndpointt.setInternalEndpoint(internal);
                    serverEndpointt.setExternalEndpoint(external);

                    this.setState({selectedPenniDinhHubExternalHostName: external, currentHubId: hub['id']});

                    if (successCallback) { successCallback(); }
                }.bind(this));
            }.bind(this));
        }.bind(this);
    }

    goToPage = (page) => {
        return function() {
            this.setState({currentPage: page, currentSubComponent: null});
        }.bind(this)
    }

    goToAddEndpoint = () => {
        this.setState({currentSubComponent: 'AddEndpoint'});
    }

    homeButtonPressed = () => {
        if (this.state && this.state.selectedPenniDinhHubExternalHostName && !this.state.isAuthorizedForSelectedHub) {
            // spinner widget during authorization shown
            this.logout();
        } else {
            this.goToPage('home')();
        }
    }

    openSideMenu = () => {
        this.setState({sideMenuOpen: !(this.state && this.state.sideMenuOpen)});
    }

    submitNewHub = (name, domain, id) => {
        SecureStore.getItemAsync(HUBS_KEY).then(function (hubs) {
            if (!hubs) {
                hubs = [];
            } else {
                hubs = JSON.parse(hubs);
            }

            hubs.push({name: name, domain: domain, id: id});
            SecureStore.setItemAsync(HUBS_KEY, JSON.stringify(hubs)).then(function () {
                this.setState({hubs: hubs, currentSubComponent: null});
            }.bind(this));
        }.bind(this));
    }

    removeHub = (currentHub) => {
        SecureStore.getItemAsync(HUBS_KEY).then(function (hubs) {
            if (!hubs) {
                hubs = [];
            } else {
                hubs = JSON.parse(hubs);
            }

            const newHubs = [];
            for (const hub of hubs) {
                if (hub['id'] !== currentHub['id']) {
                    newHubs.push(hub);
                }
            }

            SecureStore.setItemAsync(HUBS_KEY, JSON.stringify(newHubs)).then(function () {
                this.setState({hubs: newHubs, currentSubComponent: null, currentHubId: null});
                this.resetEndpoint();
            }.bind(this));
        }.bind(this));
    }

    currentHub = () => {
        if (!this.state) {
            return null;
        }

        for (let hub of this.state.hubs) {
            if (hub['id'] === this.state.currentHubId) {
                return hub;
            }
        }

        console.log('error: couldnt find hub with ID: ' + this.state.currentHubId);
        return null;
    }

    removeHubFunction = (currentHub) => {
        return function() {
            Alert.alert(currentHub['name'], 'Remove Hub Configuration', [{
                text: 'Remove',
                onPress: () => {
                    this.removeHub(currentHub);
                }
            }, {text: 'Cancel'}], {cancelable: true});
        }.bind(this);
    }

    render() {
      let webviewContent = null;
      if (this.state && this.state.openWebview) {
          webviewContent = (<WebView source={{uri: this.state.openWebview}} />);
      }

      let returnObj;
      if (this.state && this.state.currentSubComponent === 'AddEndpoint')
      {
          returnObj = <AddEndpointComponent goBack={this.goToPage('home')} submitNewHub={this.submitNewHub}/>;
      }
      else if (this.state == null || !this.state.selectedPenniDinhHubExternalHostName)
      {
          let views = [];
          if (this.state && this.state.hubs) {
              for (let hub of this.state.hubs) {
                  let view = <View key={hub['domain']} style={{
                      marginBottom: 25,
                      backgroundColor: '#6cd938',
                      width: '80%',
                      left: '10%',
                      borderRadius: 10
                  }}><Button key={hub['domain']} color='black' title={hub['name'] + ' PenniDinh'}
                             onPress={this.chooseEndpoint(hub)}/></View>;
                  views.push(view);
              }
          }
          const addEndpointButton = <View key={'addEndpointButtonView'} style={{
              marginTop: 25,
              backgroundColor: '#d1e9fe',
              width: '80%',
              left: '10%',
              borderRadius: 10
          }}><Button key={'addEndpointButton'} color='black' title={'+ Add PenniDinh Hub'}
                     onPress={this.goToAddEndpoint}/></View>;
          views.push(addEndpointButton);

          return (<ImageBackground source={dashboardBackground}
                                   style={{resizeMode: 'stretch', height: '100%'}}><ScrollView style={{paddingTop: 50 }}>{views}</ScrollView></ImageBackground>);
      }
      else if (!this.state.isAuthorizedForSelectedHub)
      {
          if (this.state.isAuthenticatingWithSelectedHub || this.state.hasAccessTokenFromSelectedPenniDinhHub) {
              let endpointMessage;
              if (this.state.endpointInUsePrettyName) {
                  endpointMessage = this.state.isAuthenticatingWithSelectedHub ? 'Authenticating with selected PenniDinh Hub' : 'Checking Authorization with selected PenniDinh Hub';
              } else {
                  endpointMessage = '';
              }

              returnObj = (<View style={{paddingLeft: 20, paddingTop: 20, paddingRight: 20}}>
                  <ScrollView style={{height: '100%'}}>
                      <View style={{paddingTop: 10, paddingLeft: 5, paddingRight: 5, paddingBottom: 20}}><Text style={{fontSize: 15, textAlign: 'center'}}>{this.state.endpointInUsePrettyName ? this.state.endpointInUsePrettyName : 'determining endpoint...'}</Text></View>
                      <View style={{paddingTop: 10, paddingLeft: 5, paddingRight: 5, paddingBottom: 20}}><Text style={{fontSize: 15, textAlign: 'center'}}>{endpointMessage}</Text></View>
                      <View style={{height: '100%'}}><WaitingSpinner /></View>
                      <View><Text style={{textAlign: 'center'}}>Copyright © 2018 - Features of this app are intellectual property of Ngoc Anh Dinh and Andrew Penniman. Features of this app are protected by copyright. Re-use of these ideas without explicit written permission from either Ngoc Anh or Andrew is strictly prohibited</Text></View>
                  </ScrollView></View>);
          } else {
              return (<ImageBackground source={dashboardBackground} style={{resizeMode: 'stretch', height: '100%'}}>
                  <View style={{top: 40, fontSize: 20, marginBottom: 0, width: '80%', left: '10%'}}><Text style={{textAlign: 'center'}}>{this.currentHub() ? this.currentHub()['name'] : ''}</Text></View>
                  <View style={{top: 50, fontSize: 10, marginBottom: 10, width: '80%', left: '10%'}}><Text style={{textAlign: 'center'}}>{this.currentHub() ? this.currentHub()['domain'] : ''}</Text></View>
                  <View style={{top: 50, backgroundColor: '#ffcccb', width: '80%', left: '10%', borderRadius: 10}}><Button color='black' title={"Remove Hub Configuration"} onPress={this.removeHubFunction(this.currentHub())} /></View>
                  <View style={{top: 200, marginBottom: 25, backgroundColor: '#6cd938', width: '80%', left: '10%', borderRadius: 10}}><Button color='black' title={"Connect with Amazon"} onPress={this.loginWithAmazon} /></View>
                  <View style={{top: 200, marginBottom: 25, backgroundColor: '#6cd938', width: '80%', left: '10%', borderRadius: 10}}><Button color='black' title={"Connect with Google"} onPress={this.loginWithGoogle} /></View>
                  <View style={{top: 200, marginBottom: 25, backgroundColor: '#6cd938', width: '80%', left: '10%', borderRadius: 10}}><Button color='black' title={"Connect with Facebook"} onPress={this.loginWithFacebook} /></View>
                  <View style={{top: 200, marginTop: 75, backgroundColor: '#6cd938', width: '80%', left: '10%', borderRadius: 10}}><Button color='black' title={"Go Back"} onPress={this.resetEndpoint} /></View>
              </ImageBackground>);
          }
      }
      else if (this.state != null && this.state.currentPage === 'lights')
      {
          returnObj = (<Lights />);
      }
      else if (this.state != null && this.state.currentPage === 'homeSecurity')
      {
          returnObj = (<HomeSecurity openWebview={this.openWebview} />);
      }
      else if (this.state != null && this.state.currentPage === 'homeClimate')
      {
          returnObj = (<HomeClimate />);
      }
      else if (this.state != null && this.state.currentPage === 'homeAlarms')
      {
          returnObj = (<HomeAlarms />);
      }
      else if (this.state != null && this.state.currentPage === 'powerOutlets')
      {
          returnObj = (<HomePlugs />);
      }
      else if (this.state != null && this.state.currentPage === 'homeHeaters')
      {
          returnObj = (<HomeHeaters />);
      }
      else if (this.state != null && this.state.currentPage === 'fileUploads')
      {
          returnObj = (<FileUploads openWebview={this.openWebview} />);
      }
      else if (this.state != null && this.state.currentPage === 'info')
      {
          returnObj = (<InfoComponent />);
      }
      else if (this.state != null && this.state.currentPage === 'VPN')
      {
          returnObj = (<VPN />);
      }
      else if (this.state != null && this.state.currentPage === 'ftp')
      {
          returnObj = (<Ftp />);
      }
      else if (this.state != null && this.state.currentPage === 'users')
      {
          returnObj = (<Users />);
      }
      else if (this.state.currentPage == null || this.state.currentPage === 'home')
      {
          const homeSecurityButton = <NavBubbleElement key={'homeSecurity'} pressAction={this.goToPage('homeSecurity')} imageSource={cameraIcon} />;
          const fileUploadsButton = <NavBubbleElement key={'fileUploads'} pressAction={this.goToPage('fileUploads')} imageSource={filesIcon} />;
          const userManagementButton = <NavBubbleElement key={'users'} pressAction={this.goToPage('users')} imageSource={usersIcon} />;
          const vpnManagementButton = <NavBubbleElement key={'vpn-management'} pressAction={this.goToPage('VPN')} imageSource={vpnIcon} />;
          const infoButton = <NavBubbleElement key={'info'} pressAction={this.goToPage('info')} imageSource={infoIcon} />;
          const ftpButton = <NavBubbleElement key={'ftp'} pressAction={this.goToPage('ftp')} imageSource={cameraSetupIcon} />;

          const people = (<Name></Name>);

          const buttons = [homeSecurityButton, fileUploadsButton, userManagementButton, vpnManagementButton, infoButton, ftpButton ];
          const buttonsSection = (<ScrollView style={{height: '100%'}}><View style={{paddingTop: 10, paddingLeft: 5, paddingRight: 5, paddingBottom: 20}}><Text style={{fontSize: 15, textAlign: 'center'}}>{this.state.endpointInUsePrettyName}</Text><Text style={{fontSize: 40, textAlign: 'center'}}>WELCOME HOME</Text><Text style={{fontSize: 20, textAlign: 'center'}}>{people}</Text></View><View style={{justifyContent: 'space-between', flexDirection: 'row', flexWrap: 'wrap'}}>{buttons}
          </View><View><Text style={{textAlign: 'center'}}>Copyright © 2018 - Features of this app are intellectual property of Ngoc Anh Dinh and Andrew Penniman. Features of this app are protected by copyright. Re-use of these ideas without explicit written permission from either Ngoc Anh or Andrew is strictly prohibited</Text></View></ScrollView>);

          returnObj = <View style={{paddingLeft: 20, paddingTop: 20, paddingRight: 20}}>{buttonsSection}</View>;

      }

      return <View style={{width: '100%', height: '100%'}}>
                  <Modal style={{}} transparent={false} visible={this.state.sideMenuOpen === true} animationType='fade' onRequestClose={this.openSideMenu}>
                      <View style={{marginTop: 50, marginBottom: 25, backgroundColor: '#6cd938', width: '80%', left: '10%', borderRadius: 10}}><Button color='black' title={'Logout Current User (signed in through ' + this.state.currentProvider + ') from ' + serverEndpointt.getExternalEndpoint()} onPress={this.logoutForgetKeys}/></View>
                      <View style={{marginBottom: 25, backgroundColor: '#6cd938', width: '80%', left: '10%', borderRadius: 10}}><Button color='black' title={'Switch Hubs (and stay logged in)'} onPress={this.switchHubs}/></View>
                      <View style={{marginTop: 100, backgroundColor: '#6cd938', width: '80%', left: '10%', borderRadius: 10}}><Button color='black' title={'Go Back'} onPress={this.openSideMenu}/></View>

                      <AppBorder homeButtonPressed={this.openSideMenu} />
                  </Modal>
                <ImageBackground source={dashboardBackground}
                                 style={{resizeMode: 'stretch', height: '100%'}}>
                   <View>
                     {webviewContent}
                   </View>
                   <View>
                     {returnObj}
                   </View>
                   <AppBorder homeButtonPressed={this.homeButtonPressed} hamburgerButtonPressed={this.state && this.state.selectedPenniDinhHubExternalHostName ? this.openSideMenu : null} />
                </ImageBackground>
             </View>;
   }
}

const styles = StyleSheet.create({
});

AppRegistry.registerComponent("main", () => App);
