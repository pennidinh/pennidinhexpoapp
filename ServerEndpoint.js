const safeJsonStringify = require('safe-json-stringify');
import NetInfo from "@react-native-community/netinfo";
import * as SecureStore from 'expo-secure-store';

var endpointToUse = null;
var proxyEndpointPort = null;

module.exports.setInternalEndpoint = (internalEndpoint) => {
    this.internalEndpoint = internalEndpoint;
};

module.exports.clearInUseEndpoint = () => {
    this.endpointToUse = null;
    this.proxyEndpointPort = null;
}

module.exports.setExternalEndpoint = (externalEndpoint) => {
   this.externalEndpoint = externalEndpoint;
   // temporary hack. make explicit clear method.
   if (!this.externalEndpoint) {
       this.endpointToUse = null;
       this.proxyEndpointPort = null;
   }
};

module.exports.getExternalEndpoint = () => {
    return this.externalEndpoint;
};

module.exports.getEndpointInUsePrettyName = () => {
    if (this.endpointToUse === null) {
        return '';
    }

    return this.proxyEndpointPort ? this.externalEndpoint + ' (via shared proxy)' : this.endpointToUse;
};

endpointInUseListenerCallbacks = {};

module.exports.registerEndpointInUseListener = (key, callback) => {
    endpointInUseListenerCallbacks[key] = callback;
};

const endpointInUseChanged = function() {
    for (var key in endpointInUseListenerCallbacks) {
        endpointInUseListenerCallbacks[key](this.endpointToUse);
    }
};

const proxyEndpointSecureStoreKey = 'PROXY_ENDPOINT_KEY_9';
const fetchAndStoreNewProxyEndpoint = function(externalEndpoint, callback, iteration = 0) {
    fetch('https://proxysockets.api.pennidinh.com/getProxyEndpoint?protocol=https&externalHostName=' + encodeURIComponent(this.externalEndpoint))
        .then(function(res) {
            console.log('returned response: ' + res.status);
            if (res.status === 200) {
                res.json()
                    .then(function (data) {
                        data['lastRefreshed'] = new Date().getTime() / 1000;
                        SecureStore.setItemAsync(proxyEndpointSecureStoreKey + externalEndpoint, safeJsonStringify(data))
                            .then(function () {
                                callback(data);
                            });
                    })
                    .catch(function(err) {
                        console.log(err);
                        callback(null);
                    });

            } else {
                callback(null);
            }
        })
        .catch(function(err) {
            console.log(err);
            console.log('fetchAndStoreNewPRoxyEndpoint failed on iteration: ' + iteration);
            if (iteration < 3) {
                setTimeout(function() {
                    fetchAndStoreNewProxyEndpoint(callback + externalEndpoint, iteration + 1);
                }, Math.exp(iteration) * 100)
            }
        })
}

const getProxyEndpoint = function(externalEndpoint, callback) {
    SecureStore.getItemAsync(proxyEndpointSecureStoreKey + externalEndpoint).then(function(result) {
        if (result) {
            const resultParsed = JSON.parse(result);

            if (resultParsed['lastRefreshed'] + (60 * 5) < (new Date().getTime() / 1000)) {
                console.log('Proxy endpoint found but stale. fetching new one...');

                fetchAndStoreNewProxyEndpoint(externalEndpoint, callback);
            } else {
                console.log('Proxy endpoint used: ' + safeJsonStringify(result));
                callback(resultParsed);
            }
        } else {
            console.log('No proxy endpoint yet stored. Fetching it now...');
            fetchAndStoreNewProxyEndpoint(externalEndpoint, callback);
        }
    });
}

module.exports.get = (callback) => {
    if (!this.internalEndpoint || !this.externalEndpoint) {
        console.error('Attempted to get an endpoint when a pennidinh hub has not been set!');
        callback(null, null);
    }

    if (this.endpointToUse == null) {
        var aRequestCompleted = false;

        const sendPings = () => {
            fetch('https://' + this.internalEndpoint + '/unsecure/ping.html')
                .then(function(res) {
                    if (res.status == 200 && !aRequestCompleted) {
                        aRequestCompleted = true;
                        this.endpointToUse = this.internalEndpoint;
                        this.proxyEndpointPort = null;

                        endpointInUseChanged();
                        callback(this.endpointToUse, null);
                    }
                })
                .catch(err => {});
            fetch('https://' + this.externalEndpoint + '/unsecure/ping.html')
                .then(function(res) {
                    if (res.status == 200 && !aRequestCompleted) {
                        aRequestCompleted = true;
                        this.endpointToUse = this.externalEndpoint;
                        this.proxyEndpointPort = null;

                        endpointInUseChanged();
                        callback(this.endpointToUse, null);
                    }
                })
                .catch(err => {});
            getProxyEndpoint(this.externalEndpoint, function(proxyEndpoint) {
                if (!proxyEndpoint) {
                    console.log('No proxy endpoint available at the moment...');
                    return;
                }

                let headers = new Headers();
                headers.append('Cookie', 'pennidinhProxyAddress=' + proxyEndpoint.port);
                fetch('https://' + proxyEndpoint.hostname + '/unsecure/ping.html', {headers: headers})
                    .then(function(res) {
                        if (res.status == 200 && !aRequestCompleted) {
                            aRequestCompleted = true;
                            this.endpointToUse = proxyEndpoint.hostname;
                            this.proxyEndpointPort = proxyEndpoint.port;

                            endpointInUseChanged();
                            callback(this.endpointToUse, this.proxyEndpointPort);
                        }
                    })
                    .catch(err => {});
            })
        };

        sendPings();

        setTimeout(function() {
            if (!aRequestCompleted) {
                sendPings();
            }
        }, 2000);

        setTimeout(function() {
            if (!aRequestCompleted) {
                aRequestCompleted = true;
                callback(null, null);
            }
        }, 10000);
    } else {
        callback(this.endpointToUse, this.proxyEndpointPort);
    }
}

NetInfo.addEventListener(state => {
  this.endpointToUse = null;
  if (this.internalEndpoint && this.externalEndpoint) {
      module.exports.get(function () {});
  }
});
