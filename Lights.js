import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image, Switch, RefreshControl, Button, TouchableOpacity } from 'react-native';
import Slider from 'react-native-slider';
import TimerMixin from 'react-timer-mixin';
const uuidv1 = require('uuid/v1');
const queryString = require('query-string');
const safeJsonStringify = require('safe-json-stringify');
import WaitingSpinner from './WaitingSpinner';
const accessTokens = require('./PenniDinhAccessTokens');
const serverEndpoint = require('./ServerEndpoint');
const sliderImage = require('./assets/images/Pennidinh_App_Logo_v01_smaller.png');
import { Asset } from 'expo-asset';
Asset.loadAsync([sliderImage]);

function getAllLightsUrl(callback) {
    serverEndpoint.get(function(serverEndpoint, proxyPort) {
       callback('https://' + serverEndpoint + '/secure-node-js/getLights.json', proxyPort);
    });
};
                       
function getSetLightUrl(callback) {
    serverEndpoint.get(function(serverEndpoint, proxyPort) {
       callback('https://' + serverEndpoint + '/secure-node-js/setLight.json', proxyPort);
    });
};

class LightManager {
    constructor(lightIndex) {
        this.lightIndex = lightIndex;
    }
    
    persistStateToLights(state) {
        var stateToUpdate = {lightIndex: this.lightIndex};
        if (state.power != null)
        {
            stateToUpdate.on = state.power;
        }
        if (state.brightness != null)
        {
            stateToUpdate.bri = Math.round(state.brightness);
        }
                       
        getSetLightUrl(function(url, proxyPort) {
            var queryStringResult = url + '?' + queryString.stringify(stateToUpdate);
            console.log('URL query string result for setting light state: ' + queryStringResult);
            
            accessTokens.get((accessToken) => {
              fetch(queryStringResult, {headers: {'Cookie': ('expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort)}, method: 'post'})
                .then((res) => res.json())
                .catch(err => { throw new Error(err) })
                .then((res) => res)
                .catch(err => { throw new Error(err) });
              });
        });
    }
}

class Light extends React.Component {
    constructor(props) {
        super(props);
        
        if (this.state == null)
        {
            this.state = {};
            this.state.power = false;
            this.state.brightness = 0;
            this.state.reachable = true;
            this.state.refreshing = null;
        }
    }
    
    async componentDidMount() {
        await this.setState({power: this.props.json.state.on, brightness: this.props.json.state.bri, reachable: this.props.json.state.reachable});
    }
    
    setBrightness(brightnessValue) {
        this.setState({brightness: brightnessValue}, this.persistStateToLights);
    }
    
    setPower(powerValue) {
        this.setState({power: powerValue}, this.persistStateToLights);
    }
    
    persistStateToLights() {
        this.props.lightManager.persistStateToLights(this.state);
    }
    
    render() {
        let icons = [];
        
        if (this.state.reachable === true) {
            icons.push((<Image key={uuidv1()} style={styles.connected} source={{uri: "https://cdn.tutsplus.com/mobile/uploads/2013/07/iOS-SDK_Reachability_Network-Tower2x.png"}} />));
        } else {
            icons.push(<Text style={{color: 'red'}}>Unreachable</Text>)
        }
        
        let items = icons;
        items.push((<Slider
                    //  disabled={!this.state.reachable}
                    minimumValue={0}
                    maximumValue={254}
                    value={this.state.brightness}
                    thumbImage={sliderImage}
                    thumbStyle={{width: 0, height: 0, left: -16, top: 5}}
                    onSlidingComplete={(value) => this.setBrightness(value)} />));
        
        let lightPowerSwitch = <Switch
        key={uuidv1()}
        //  disabled={!this.state.reachable}
        style={styles.switchButton}
        value={this.state.power}
        onValueChange={(value => this.setPower(value))}/>;
        
        return (<View style={styles.light}><View style={{flex: 2, flexDirection: 'row' }}><Text key={uuidv1()} style={styles.title}>{this.props.json.name}</Text>{lightPowerSwitch}</View><Text>{"\n"}</Text>{items}</View>);
    }
}

function allLights(lightsAsReactComponents, handleRefreshFunction, onOrOffBoolean) {
    var i = 0;
    
    lightsAsReactComponents
        .forEach((light) => {
             setTimeout(
                 function() {
                     light.setPower(onOrOffBoolean);
                     i = i + 1;
                 }
             , i * 20);
        });

    setTimeout(handleRefreshFunction, 750);
}

export default class AllLights extends React.Component {
    constructor(props) {
        super(props);
        
        if (this.state == null)
        {
            this.state = {};
            this.state.refreshing = false;
            this.state.jsonData = null;
        }
    }
    
    
    async componentDidMount() {
        return this.handleRefresh();
    }
    
    handleRefresh() {
        this.helperHandleRefresh();
    }
    
    helperHandleRefresh() {
        this.setState({refreshing: true});
        var that = this;
        accessTokens.get((accessToken) => {
             getAllLightsUrl(function(url, proxyPort) {
                  fetch(url, {headers: {'Cookie': ('expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort)}})
                    .then((res) => res.json())
                    .then((res) => {
                      that.setState({refreshing: false, jsonData: res});
                      })
                    .catch(error => console.log('error: ' + JSON.stringify(error)));
             });
        });
    }
    
    allLightsOff() {
        allLights(this.lightsAsReactComponents, this.handleRefresh.bind(this), false);
    }
    
    allLightsOn() {
        allLights(this.lightsAsReactComponents, this.handleRefresh.bind(this), true);
    }
    
    render() {
        if (this.state !== null && this.state.jsonData !== null)
        {
            let iteratorCounter = 0;
            this.lightsAsReactComponents = [];
            var lights = Object.keys(this.state.jsonData)
            .map(lightIndex => (<Light ref={(c) => {this.lightsAsReactComponents[lightIndex] = c}} lightManager={new LightManager(lightIndex)} key={uuidv1()} iteratorCounter={iteratorCounter++} json={this.state.jsonData[lightIndex]} />));
            
            var lightsOffButton = (<TouchableOpacity style={{backgroundColor: 'lightgrey', width: 150, borderRadius:10,borderWidth: 3, borderColor: 'lightslategrey'}} onPress={this.allLightsOff.bind(this)}><Button onPress={this.allLightsOff.bind(this)} title='All Lights Off'></Button></TouchableOpacity>);
            var lightsOnButton = (<TouchableOpacity style={{backgroundColor: 'lightgrey', width: 150, borderRadius:10,borderWidth: 3, borderColor: 'lightslategrey'}} onPress={this.allLightsOn.bind(this)}><Button onPress={this.allLightsOn.bind(this)} title='All Lights On'></Button></TouchableOpacity>);
            var buttons = (<View style={{flex: 2, flexDirection: 'row', justifyContent:'space-between', padding: 5}}>{lightsOffButton}{lightsOnButton}</View>);
            
            return (<ScrollView refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.handleRefresh}/>} key={uuidv1()} style={styles.container}>{buttons}{lights}</ScrollView>);
        }
        else
        {
            return  <WaitingSpinner />;
        }
    }
}


const styles = StyleSheet.create({
                                 container: {
                                 paddingTop: 30,
                                 flex: 1,
                                 backgroundColor: '#fff'
                                 },
                                 title: {
                                 fontSize: 25,
                                 paddingRight: 20
                                 },
                                 connected: {
                                 width: 30,
                                 height: 30
                                 },
                                 switchButton: {
                                 paddingBottom: 0
                                 },
                                 light: {
                                 paddingTop: 2,
                                 paddingBottom: 2,
                                 borderTopWidth: 1,
                                 borderBottomWidth: 1,
                                 borderColor: 'lightgrey'
                                 }
                                 });
