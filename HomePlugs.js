import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image, Switch, RefreshControl, Button, TouchableOpacity, WebView } from 'react-native';
import Slider from 'react-native-slider';
import TimerMixin from 'react-timer-mixin';
const uuidv1 = require('uuid/v1');
const safeJsonStringify = require('safe-json-stringify');
const base64 = require('base-64');
import WaitingSpinner from './WaitingSpinner';
const accessTokens = require('./PenniDinhAccessTokens');
const serverEndpoint = require('./ServerEndpoint');

class PowerOutlet extends React.Component {
    togglePower = () => {
        var that = this;
        accessTokens.get((accessToken) => {
          serverEndpoint.get(function(serverEndpoint, proxyPort) {
              let headers = new Headers();
              headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
              fetch("https://" + serverEndpoint + "/secure-node-js/setPlugDevice.json?hostIP=" + that.props.device.outletIP + "&hostPort" + that.props.device.outletPort, {headers: headers, method: 'post'})
                .then(res => setTimeout(that.props.homePlugsController.refreshData, 500))
                .catch(err => alert(err));
          });
        });
    }
    
    render() {
        var doesHaveInUseState = this.props.device.model.indexOf('HS110') > -1 && false;
        var inUseText = doesHaveInUseState ? (<Text style={{fontSize: 15}}>In Use: <Text style={{color: (this.props.device.powerState ? 'green' : 'red')}}>{ this.props.device.powerState ? 'On' : 'Off'}</Text></Text>) : (<Text style={{fontSize: 10, color: 'gray'}}>No "in use" state for this device type</Text>);
        
        return (<View style={{paddingTop: 20, paddingBottom: 15}}>
                  <Text>{this.props.device.productName}</Text>
                  <Text style={{fontSize: 20}}>{this.props.device.friendlyName}</Text>
                  {inUseText}
                  <Text style={{fontSize: 15}}>Power: <Text style={{color: (this.props.device.relayState ? 'green' : 'red')}}>{ this.props.device.relayState ? 'On' : 'Off'}</Text></Text>
                <Button title='Toggle Power' onPress={this.togglePower} />
                </View>);
    }
}

export default class HomePlugsController extends React.Component {
    async setDevices(devicesResponse) {
        this.setState({devices: devicesResponse});
    }
    
    refreshData = () =>
    {
        var that = this;
        accessTokens.get((accessToken) => {
          serverEndpoint.get(function(serverEndpoint, proxyPort) {
              let headers = new Headers();
              headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
              fetch("https://" + serverEndpoint + "/secure-node-js/getPlugDevices.json", {headers: headers})
                .then(res => res.json())
                .then(res => that.setDevices(res))
                .catch(err => alert(err));
          });
        });
    }
    
    async componentDidMount() {
        this.refreshData();
    }
    
    render() {
        if (this.state != null && this.state.devices != null)
        {
            let devices = this.state.devices
            .sort(function(a, b) {
                  if (a.outletIP > b.outletIP)
                   return 1;
                  else if (b.outletIP > a.outletIP)
                   return -1;
                  else
                   return 0;
            })
            .map(device => (<PowerOutlet homePlugsController={this} device={device} />));
            
            return (<View style={{flex: 1, padding: 20}}>{devices}</View>);
        }
        else
        {
            return <WaitingSpinner />;
        }
    }
}
