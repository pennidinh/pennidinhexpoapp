import React from "react";
import { SecurityCameraImages, NumberOfImagesPicker } from './media/Tile';
import WaitingSpinner from './WaitingSpinner';
import { View, ScrollView, RefreshControl, Button, Linking } from 'react-native';
const playIcon = require('./assets/images/playIcon.png');
import { Asset } from 'expo-asset';
Asset.loadAsync([playIcon]);
const accessTokens = require('./PenniDinhAccessTokens');
const serverEndpoint = require('./ServerEndpoint');
const safeJsonStringify = require('safe-json-stringify');

const picturesPerPageNums = [];
picturesPerPageNums.push('2');
picturesPerPageNums.push('4');
picturesPerPageNums.push('6');
picturesPerPageNums.push('12');
picturesPerPageNums.push('16');
for (var i = 2; i < 1000; i ++) {
    picturesPerPageNums.push((i * 10) + '');
}

export default class FileUploads extends React.Component {
    async componentDidMount() {
        return this.handleRefresh();
    }

    handleRefresh = () => {
        this.setState({refreshing: true});
        var that = this;
        accessTokens.get((accessToken) => {
            serverEndpoint.get(function(serverEndpoint, proxyPort) {
                let headers = new Headers();
                headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
                fetch("https://" + serverEndpoint + "/secure-node-js/uploadFiles.json", {headers: headers})
                    .then(res => res.json())
                    .then((res) => {
                        var cameras = Array.from(new Set(res.map((val) => val.filePath.split('/')[3])));
                        var securityFiles = res.map(val => val.filePath);
                        var securityFileObjects = res
                            .map(val => {
                                var mediaType;
                                if (val.filePath.indexOf('jpg') > -1 || val.filePath.indexOf('JPG') > -1 || val.filePath.indexOf('jpeg') > -1 || val.filePath.indexOf('JPEG') > -1  || val.filePath.indexOf('png') > -1  || val.filePath.indexOf('PNG') > -1) {
                                    mediaType = "image";
                                } else if (val.filePath.indexOf('mp4') > -1 || val.filePath.indexOf('MP4') > -1 || val.filePath.indexOf('MOV') > -1 || val.filePath.indexOf('mov') > -1) {
                                    mediaType = "video";
                                } else {
                                    console.log("Couldn't determine media type of :" + val.filePath);
                                    return null;
                                }
                                const urnPath = (val.filePath.startsWith('/home-security') ? val.filePath.substring('/home-security'.length) : val.filePath).replaceAll(' ', '%20');

                                return {urnPath: urnPath, creationTimeMs: val.creationTimeMs, mediaType: mediaType};
                            })
                            .filter(item => item != null);

                        that.setState({refreshing: false, homeSecurityFiles: securityFiles, cameras: cameras, securityFileObjects: securityFileObjects});
                    })
                    .catch(err => console.log(err));
            });
        });
    }

    uploadFile = () => {
        accessTokens.get((accessToken) => {
            serverEndpoint.get(function (serverEndpoint, proxyPort) {
              if (proxyPort) {
                Linking.openURL('https://' + serverEndpoint + '/cgi-bin/setProxyEndpoint.cgi?expiringToken=' + encodeURIComponent(accessToken) + '&pennidinhProxyPort=' + proxyPort + '&redirectTo=' + encodeURIComponent('/secure/upload.html'));
              } else {
                Linking.openURL('https://' + serverEndpoint + '/secure-node-js/setLoginCookieAndRedirect.js?accessToken=' + encodeURIComponent(accessToken) + '&redirect_to=' + encodeURIComponent('https://' + serverEndpoint + '/secure/upload.html'));
              }
            });
        });
    }

    render() {
        if (this.state == null || this.state.homeSecurityFiles == null) {
            return (<WaitingSpinner />);
        } else if (this.state != null && this.state.homeSecurityFiles != null) {
            const securityFileObjects = this.state.securityFileObjects.filter(securityFileObject => securityFileObject.mediaType === 'video' || securityFileObject.mediaType === 'image');
            var maxNumPages = [];
            var imagesPerPage = this.state.imagesPerPage != null ? this.state.imagesPerPage : 12;
            for(var i = 0; i < Math.ceil(securityFileObjects.length / imagesPerPage); i++){
                maxNumPages.push(i + 1);
            }

            var uploadFileObjectsFilteredAndSorted = securityFileObjects.sort((a, b) => {
                return b.creationTimeMs - a.creationTimeMs;
            });

            var pageNum = this.state.pageNum != null ? this.state.pageNum : 1;
            var uploadFileObjectsFilteredSortedAndPaged = uploadFileObjectsFilteredAndSorted
                .slice((pageNum - 1) * imagesPerPage, pageNum * imagesPerPage);

            return (<View>
                <ScrollView style={{paddingTop: 30}} refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.handleRefresh}/>}>
                    <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                        <NumberOfImagesPicker labelPrefix={'Page '} labelSuffix={''} numbers={maxNumPages} setNumber={(newNumber) => this.setState({pageNum: newNumber})} initialValue={pageNum + ''} />
                        <NumberOfImagesPicker labelPrefix={''} labelSuffix={' files'} numbers={picturesPerPageNums} setNumber={(newNumber) => this.setState({imagesPerPage: newNumber})} initialValue={imagesPerPage + ''} />
                    </View>
                <View  style={{backgroundColor: '#6cd938', width: '80%', left: '10%', borderRadius: 10, top: 20}}><Button color='black' title={"Upload File"} onPress={this.uploadFile} /></View>
                    <SecurityCameraImages openWebview={this.props.openWebview} update={this.handleRefresh} canMakePublic={true} totPages={Math.ceil(uploadFileObjectsFilteredAndSorted.length / imagesPerPage)} pageNum={pageNum} key={'/'} homeSecurityFileObjectsFilteredAndSortedAndPagedForCamera={uploadFileObjectsFilteredSortedAndPaged} prettyName={'/'} imagesPerCamera={imagesPerPage} />

                 <View style={{height: 75}} />
                </ScrollView>
            </View>);
        }
    }
}
