import React from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
const safeJsonStringify = require('safe-json-stringify');
const base64 = require('base-64');

export default class WaitingSpinner extends React.Component {
    render() {
        return <View style={{alignItems: 'center', top: '50%'}}><ActivityIndicator size={'large'} ></ ActivityIndicator></View>;
    }
}
