import React from 'react';
import { Clipboard, Alert, StyleSheet, Text, View, ScrollView, Switch, RefreshControl, Image, Button, TouchableOpacity, WebView, FlatList, Linking } from 'react-native';
import { Picker } from '@react-native-picker/picker'
const playIcon = require('../assets/images/playIcon.png');
import * as FileSystem from 'expo-file-system';
import { Asset } from 'expo-asset';
Asset.loadAsync([playIcon]);
const accessTokens = require('../PenniDinhAccessTokens');
const serverEndpoint = require('../ServerEndpoint');
import { CacheManager, type DownloadOptions } from "react-native-expo-image-cache";
const toUpdate = {};
const counter = {};
counter['i'] = 0;
const timer = {};
const reloadImages = {};

const updateImages = function() {
    const i = counter['i'];
    const toUpdateKeys = Object.keys(toUpdate);

    const maxItemsToUpdate = Math.min(16, toUpdateKeys.length);

    const toUpdateIterationList = i + maxItemsToUpdate > toUpdateKeys.length
        ? toUpdateKeys.slice(i, toUpdateKeys.length).concat(toUpdateKeys.slice(0, (i + maxItemsToUpdate) % toUpdateKeys.length))
        : toUpdateKeys.slice(i, i + maxItemsToUpdate);

    toUpdateIterationList.forEach(toUpdateKey => {
        try {
            toUpdate[toUpdateKey]();
        } catch (err) {
            console.log(err);
        }
    });
    counter['i'] = ((i + 1) % (Math.max(toUpdateKeys.length , 1)));
};

const refreshImages = function() {
    const reloadImagesKeys = Object.keys(reloadImages);
    for (let i = 0; i < reloadImagesKeys.length; i++) {
        reloadImages[reloadImagesKeys[i]]();
    }
}

class RotatingImage extends React.Component {
    loadFiles = (i, accessToken) => {
        var that = this;
        serverEndpoint.get(function(serverEndpoint, proxyPort) {
            var thumbnailUri = 'https://' + serverEndpoint + '/secure/' + that.props.homeSecurityFileObject.urnPath.replace('\.mov', '-thumbnail-0000' + (i + 1) + '\.png').replace('\.MOV', '-thumbnail-0000' + (i + 1) + '\.png').replace('\.MP4', '-thumbnail-0000' + (i + 1) + '\.png').replace('\.mp4', '-thumbnail-0000' + (i + 1) + '\.png');
            CacheManager.get(thumbnailUri, {headers: {
                    Cookie: ('expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort)
                }}).getPath()
                .then(res => {
                    if (res == null) {
                        console.log('no thumbnail found for ' + that.props.homeSecurityFileObject.urnPath);
                        return;
                    }
                    FileSystem.readAsStringAsync(res, {encoding: FileSystem.EncodingType.Base64})
                        .then((res) => {
                            if (res == null) {
                                console.log('no thumbnail found for ' + that.props.homeSecurityFileObject.urnPath);
                                return;
                            }

                            var stateUpdate = {};
                            stateUpdate['image-' + i] = (<Image
                                source={{uri: 'data:image/png;base64,' + res}}
                                onError = {(e) => console.log(e)}
                                style = {{ width: '100%', height: '100%' }} />);
                            that.setState(stateUpdate);
                        })
                        .catch((err) => console.log(err))
                })
                .catch((err) => console.log(err));
        });
    }

    async componentDidMount() {
        toUpdate[this.props.mykey] = function() {
            const currentImageIndex = (!this.state || !this.state.currentImageIndex) ? 0 : this.state.currentImageIndex;
            const nextImageIndex = ((currentImageIndex + 1) % 6);
            this.setState({currentImageIndex: nextImageIndex});
            if (this.state && this.state['image-' + Math.min(2, nextImageIndex)]) {
                this.setState({currentShowingImageIndex: Math.min(2, nextImageIndex)});
            }
        }.bind(this);

        reloadImages[this.props.mykey] = function () {
            accessTokens.get(function(accessToken) {
                this.loadFiles(0, accessToken);

                setTimeout(function() {
                    for (let i = 1; i < 3; i ++) {
                        this.loadFiles(i, accessToken);
                    }
                }.bind(this), 1000)
            }.bind(this));
        }.bind(this);
        reloadImages[this.props.mykey]();
    }

    async componentWillUnmount() {
        delete toUpdate[this.props.mykey];
        delete reloadImages[this.props.mykey];
    }

    render() {
        const currentShowingImageIndex = (this.state && this.state.currentShowingImageIndex) ? this.state.currentShowingImageIndex : 0;
        if (this.state != null && this.state['image-' + currentShowingImageIndex] != null)
        {
            return this.state['image-' + currentShowingImageIndex];
        } else {
            return (<View></View>);
        }
    }
}

class SecurityImage extends React.Component {
    async componentDidMount() {
        var that = this;
        if (this.props.homeSecurityFileObject.mediaType === "image") {
            accessTokens.get(function(accessToken) {
                serverEndpoint.get(function(serverEndpoint, proxyPort) {
                    var thumbnailUri = 'https://' + serverEndpoint + '/secure/' + that.props.homeSecurityFileObject.urnPath.replace('\.png', '-thumbnail\.png').replace('\.jpg', '-thumbnail\.png').replace('\.JPG', '-thumbnail\.png');
                    CacheManager.get(thumbnailUri, {headers: {
                            Cookie: ('expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort)
                        }}).getPath()
                        .then(res => {
                            if (res == null) {
                                console.log('no thumbnail found for ' + that.props.homeSecurityFileObject.urnPath);
                                return;
                            }
                            FileSystem.readAsStringAsync(res, {encoding: FileSystem.EncodingType.Base64})
                                .then((res) => {
                                    that.setState({image: (<Image
                                            source={{uri: 'data:image/png;base64,' + res}}
                                            onError = {(e) => console.log(e)}
                                            style = {{ width: '100%', height: '100%' }} />)});
                                })
                                .catch((err) => console.log(err))
                        })
                        .catch((err) => console.log(err));
                });
            });
        }
    }

    onTouch = () => {
        if (!this.timer && !this.disabledTouch) {
            this.showOptions();
        }
    }

    openInBrowser = () => {
        var that = this;
        accessTokens.get((accessToken) => {
            serverEndpoint.get(function(serverEndpoint, proxyPort) {
              if (proxyPort) {
                this.props.openWebview('https://' + serverEndpoint + '/cgi-bin/setProxyEndpoint.cgi?expiringToken=' + encodeURIComponent(accessToken) + '&pennidinhProxyPort=' + proxyPort + '&redirectTo=' + encodeURIComponent('/secure' + that.props.homeSecurityFileObject.urnPath));
              } else {
                this.props.openWebview('https://' + serverEndpoint + '/secure-node-js/setLoginCookieAndRedirect.js?accessToken=' + encodeURIComponent(accessToken) + '&redirect_to=' + encodeURIComponent('https://' + serverEndpoint + '/secure' + that.props.homeSecurityFileObject.urnPath));
              }
            }.bind(this));
        });
    }

    save = (isPublic) => {
        const that = this;
        accessTokens.get((accessToken) => {
          serverEndpoint.get(function(serverEndpoint, proxyPort) {
            const headers = new Headers();
            headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
            fetch("https://" + serverEndpoint + "/secure-node-js/saveSecurityFile.js?file=" + encodeURIComponent(that.props.homeSecurityFileObject.homeSecurityPath) + "&isPublic=" + isPublic, {headers: headers, method: 'post'})
                .then((res) => {
                    if (res['status'] === 200) {
                        alert(isPublic ? 'File saved and shared publicly. Find it in the file uploads section' : 'File saved. Find it in the file uploads section');
                    } else {
                        console.log(err);
                        alert('Request failed');
                    }
                })
                .catch(err => {
                    console.log(err);
                    alert('Request failed');
                });
          });
        });
    }

    makePublic = () => {
        const that = this;
        accessTokens.get((accessToken) => {
            serverEndpoint.get(function(serverEndpoint, proxyPort) {
                const headers = new Headers();
                headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
                fetch("https://" + serverEndpoint + "/secure-node-js/makeUploadFilePublic.js?file=" + encodeURIComponent(that.props.homeSecurityFileObject.urnPath), {headers: headers, method: 'post'})
                    .then((res) => {
                        if (res['status'] === 200) {
                            alert('File made public');
                        } else {
                            alert('Request failed');
                        }
                    })
                    .catch(err => {
                        console.log(err);
                        alert('Request failed');
                    });
            });
        });
    }

    deleteUploadFile = () => {
        const that = this;
        accessTokens.get((accessToken) => {
            serverEndpoint.get(function(serverEndpoint, proxyPort) {
                const headers = new Headers();
                headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
                fetch("https://" + serverEndpoint + "/secure-node-js/deleteUploadFile.js?file=" + encodeURIComponent(that.props.homeSecurityFileObject.urnPath), {headers: headers, method: 'post'})
                    .then((res) => {
                        if (res['status'] === 200) {
                            that.props.onUpdate();
                            alert('File deleted successfully');
                        } else {
                            console.log(err);
                            alert('Request failed');
                        }
                    })
                    .catch(err => {
                        console.log(err);
                        alert('Request failed');
                    });
            });
        });
    };

    deleteSecurityFile = () => {
        const that = this;
        accessTokens.get((accessToken) => {
            serverEndpoint.get(function(serverEndpoint, proxyPort) {
                const headers = new Headers();
                headers.append('Cookie', 'expiringToken=' + accessToken + ';pennidinhProxyAddress=' + proxyPort);
                fetch("https://" + serverEndpoint + "/secure-node-js/deleteSecurityFile.js?file=" + encodeURIComponent(that.props.homeSecurityFileObject.homeSecurityPath), {headers: headers, method: 'post'})
                    .then((res) => {
                        if (res['status'] === 200) {
                            that.props.onUpdate();
                            alert('File deleted successfully');
                        } else {
                            console.log(err);
                            alert('Request failed');
                        }
                    })
                    .catch(err => {
                        console.log(err);
                        alert('Request failed');
                    });
            });
        });
    };

    showOptions = () => {
      this.disabledTouch = true;
      if (this.props.canSave) {
          Alert.alert('File Options', 'What would you like to do with this file?', [{
              text: 'Open', onPress: () => {
                  this.openInBrowser();
              }
          }, {
              text: 'Save', onPress: () => {
                  this.save(false);
              }
          }, {
              text: 'Delete', onPress: () => {
                  this.deleteSecurityFile();
              }
          }, {text: 'Cancel'}], {cancelable: true});
      } else if (this.props.canMakePublic) {
          const isPublic = this.props.homeSecurityFileObject.urnPath.indexOf('/uploads/public') === 0;
          const options = [{
              text: 'Open',
              onPress: () => {
                  this.openInBrowser();
              }
          }, {text: 'Delete', onPress: () => this.deleteUploadFile()}, {text: 'Cancel'}];
          if (!isPublic) {
              options.push({
                  text: 'Make Public',
                  onPress: () => {
                      this.makePublic();
                  }
              });
          }
          Alert.alert('File Options', 'What would you like to do with this file?', options, {cancelable: true});
      }
    }

    onPressOut = () => {
      if (this.timer) {
        clearTimeout(this.timer);
        this.timer = null;
      }
    }

    onPressIn = () => {
      this.disabledTouch = false;
      if (this.props.canSave || this.props.canMakePublic) {
        this.timer = setTimeout(() => {
         // this.showOptions();
            // Do nothing for now
        }, 1000);
      }
    }

    render () {
        var element;
        if (this.props.homeSecurityFileObject.mediaType === "video") {
            element = (<RotatingImage mykey={this.props.homeSecurityFileObject.urnPath} key={this.props.homeSecurityFileObject.urnPath} homeSecurityFileObject={this.props.homeSecurityFileObject} />);
        } else if (this.state == null || this.state.image == null) {
            element = (<View></View>);
        } else {
            element = this.state.image;
        }

        return (<TouchableOpacity onPressOut={this.onPressOut} onPressIn={this.onPressIn} onPress={this.onTouch} style={{width: '100%', height: '100%'}}>
            {element}
        </TouchableOpacity>);
    }
}


function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

const createTimeRangeElementFromEpochTime = function(latestSecurityFileEpochTime, earliestSecurityFileObjectEpochTime) {
    const latestImageFileDate = new Date(0);
    latestImageFileDate.setUTCSeconds(latestSecurityFileEpochTime);

    const earliestImageFileDate = new Date(0);
    earliestImageFileDate.setUTCSeconds(earliestSecurityFileObjectEpochTime);

    const beginningText = earliestImageFileDate.toLocaleDateString("en-US", { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' }) + ' ' + formatAMPM(earliestImageFileDate);
    const endText = (earliestImageFileDate.toLocaleDateString("en-US", { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' }) === latestImageFileDate.toLocaleDateString("en-US", { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' })) ? formatAMPM(latestImageFileDate) : latestImageFileDate.toLocaleDateString("en-US", { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' }) + ' ' + formatAMPM(latestImageFileDate);

    return (<Text>{beginningText} to {endText}</Text>);
}

export class SecurityCameraImages extends React.Component {
    async componentWillUnmount() {
        if (timer['pointer']) {
            clearInterval(timer['pointer']);
            delete timer['pointer'];
        }

        if (timer['refreshImages']) {
            clearInterval(timer['refreshImages']);
            delete timer['refreshImages'];
        }
    }

    async componentDidMount() {
        if (timer['pointer']) {
            clearInterval(timer['pointer']);
            delete timer['pointer'];
        }
        timer['pointer'] = setInterval(updateImages, 500);

        if (timer['refreshImages']) {
            clearInterval(timer['refreshImages']);
            delete timer['refreshImages'];
        }
        timer['refreshImages'] = setInterval(refreshImages, 10000);
    }

    render() {
        var lastSeenInfo;
        if (this.props.earliestSecurityFileEpochTime && this.props.latestSecurityFileEpochTime) {
            var latestDateAgo = createTimeRangeElementFromEpochTime(this.props.earliestSecurityFileEpochTime, this.props.latestSecurityFileEpochTime);

            lastSeenInfo = (<Text style={{fontSize: 15}}> - {this.props.totPages > 0 ? ('page ' + this.props.pageNum + ' / ' + this.props.totPages) : ''} - {latestDateAgo}</Text>);
        } else {
            lastSeenInfo =(<Text style={{fontSize: 15, fontStyle: 'italic'}}></Text>);
        }

        return (<View><Text style={{fontSize: 20, paddingTop: 40, paddingBottom: 5, paddingLeft: 10}}>{this.props.prettyName}{lastSeenInfo}</Text><View style = {{ flexDirection: 'row', flexWrap: 'wrap'}}>{ this.props.homeSecurityFileObjectsFilteredAndSortedAndPagedForCamera
            .map(homeSecurityFileObject => <View style = {{ width: '50%', aspectRatio: 16/9 }} key={homeSecurityFileObject.urnPath} ><SecurityImage openWebview={this.props.openWebview} onUpdate={this.props.update} canMakePublic={this.props.canMakePublic} canSave={this.props.canSave} mykey={homeSecurityFileObject.urnPath} key={homeSecurityFileObject.urnPath} homeSecurityFileObject={homeSecurityFileObject} /></View>) }</View></View>);
    }
}

export class NumberOfImagesPicker extends React.Component {
    render() {
        var pickerItems = this.props.numbers.map((pickerNum) => {
            return (<Picker.Item key={pickerNum} label={this.props.labelPrefix + pickerNum + '' + this.props.labelSuffix} value={pickerNum + ''} />);
        });

        return (<Picker
            selectedValue={this.props.initialValue}
            onValueChange={(itemValue) =>
                this.props.setNumber(parseInt(itemValue))
            }
            style={{width: '25%', height: 100}} itemStyle={{height: 100}}>
            {pickerItems}
        </Picker>)
    }
}

const styles = StyleSheet.create({
});
