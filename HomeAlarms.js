import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image, Switch, RefreshControl, Button, TouchableOpacity, WebView } from 'react-native';
import Slider from 'react-native-slider';
import TimerMixin from 'react-timer-mixin';
const uuidv1 = require('uuid/v1');
const safeJsonStringify = require('safe-json-stringify');
const base64 = require('base-64');
import WaitingSpinner from './WaitingSpinner';
const accessTokens = require('./PenniDinhAccessTokens');

class SmokeCOAlarmHeader extends React.Component {
    millisecondsToStr (milliseconds) {
        // TIP: to find current time in milliseconds, use:
        // var  current_time_milliseconds = new Date().getTime();
        
        function numberEnding (number) {
            return (number > 1) ? 's' : '';
        }
        
        var temp = Math.floor(milliseconds / 1000);
        var years = Math.floor(temp / 31536000);
        if (years) {
            return years + ' year' + numberEnding(years);
        }
        //TODO: Months! Maybe weeks?
        var days = Math.floor((temp %= 31536000) / 86400);
        if (days) {
            return days + ' day' + numberEnding(days);
        }
        var hours = Math.floor((temp %= 86400) / 3600);
        if (hours) {
            return hours + ' hour' + numberEnding(hours);
        }
        var minutes = Math.floor((temp %= 3600) / 60);
        if (minutes) {
            return minutes + ' minute' + numberEnding(minutes);
        }
        var seconds = temp % 60;
        if (seconds) {
            return seconds + ' second' + numberEnding(seconds);
        }
        return 'less than a second'; //'just now' //or other string you like;
    }
    
    render() {
        let secondsSinceLastUpdate = new Date() - new Date(this.props.data.last_connection);
        
        return (<View style={{flexDirection: 'row', paddingBottom: 5}}><Text>Name: {this.props.data.name} - </Text><Text style={{color: this.props.data.ui_color_state}}> {this.props.data.is_online ? '(online)' : '(offline)'} </Text><Text> - </Text><Text>Connected: {this.millisecondsToStr(secondsSinceLastUpdate)} ago</Text></View>);
    }
}

class SmokeCOAlarmStates extends React.Component {
    buildStateText(name, value) {
        return <View style={{flexDirection: 'row', flexWrap: 'wrap'}}><Text style={{fontSize: 15}}>{name}: </Text><Text style={{color: (value == 'ok' ? 'green' : 'black'), fontSize: 15}}>{value}</Text></View>;
    }
    
    render() {
        return (<View>{this.buildStateText('Battery Health', this.props.data.battery_health)}{this.buildStateText('Carbon Monoxide Alarm', this.props.data.co_alarm_state)}{this.buildStateText('Smoke Alarm', this.props.data.smoke_alarm_state)}</View>);
    }
}

class SmokeCOAlarm extends React.Component {
    render() {
        return (<View style={{padding: 15}}><View><SmokeCOAlarmHeader data={this.props.data} /><SmokeCOAlarmStates data={this.props.data} /></View></View>);
    }
}

export default class HomeClimate extends React.Component {
    async setAccessToken(accessTokenResponse) {
        let accessToken = accessTokenResponse.access_token;
        
        console.log('accessToken: ' + accessToken);
        
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + accessToken);
        
        let response = await fetch('https://developer-api.nest.com/', {headers: headers})
        
        if (response['status'] == 401 && response['url'] != 'https://developer-api.nest.com/') {
            console.log('Unauthorized from a different URL. Trying again with auth credentials at new target...');
            
            response = await fetch(response['url'], {headers: headers});
        }
        
        let responseJson = await response.json();
        
        console.log(safeJsonStringify(responseJson));
        
        this.setState({launchOAuth: false, data: responseJson});
    }
    
    launchOAuthPage() {
        this.setState({launchOAuth: true});
    }
    
    async componentDidMount() {
        accessTokens.get((accessToken) => {
            let headers = new Headers();
            headers.append('Cookie', 'expiringToken=' + accessToken);
            
            fetch("https://pennidinh.com/secure/getAccessToken.cgi?state=NEST", {headers: headers, redirect: 'follow'})
              .then(res => res.json())
              .then(res => this.setAccessToken(res))
              .catch(err => this.launchOAuthPage());
        });
    }
    
    render() {
        if (this.state != null && this.state.launchOAuth === true)
        {
            return (<View style={{flex: 1}}><WebView    source={{uri: 'https://home.nest.com/login/oauth2?client_id=c386b4ad-798f-43f0-af74-b6da4b585bd5&state=NEST'}}
                    style={{width: '100%', height:'90%'}}/></View>);
        }
        else if (this.state != null && this.state.data != null)
        {
            let smokeAlarms = Object.keys(this.state.data.devices.smoke_co_alarms)
                .map(alarmKey => (<SmokeCOAlarm data={this.state.data.devices.smoke_co_alarms[alarmKey]}/>));
            return <ScrollView style={{paddingTop: 30}}>{smokeAlarms}</ScrollView>;
        }
        else
        {
            return <WaitingSpinner />;
        }
    }
}
