import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image, Switch, RefreshControl, Button, TouchableOpacity, WebView } from 'react-native';
import Slider from 'react-native-slider';
import TimerMixin from 'react-timer-mixin';
const uuidv1 = require('uuid/v1');
const safeJsonStringify = require('safe-json-stringify');
const base64 = require('base-64');
import WaitingSpinner from './WaitingSpinner';
const accessTokens = require('./PenniDinhAccessTokens');

class Thermostat extends React.Component {
    millisecondsToStr (milliseconds) {
        // TIP: to find current time in milliseconds, use:
        // var  current_time_milliseconds = new Date().getTime();
        
        function numberEnding (number) {
            return (number > 1) ? 's' : '';
        }
        
        var temp = Math.floor(milliseconds / 1000);
        var years = Math.floor(temp / 31536000);
        if (years) {
            return years + ' year' + numberEnding(years);
        }
        //TODO: Months! Maybe weeks?
        var days = Math.floor((temp %= 31536000) / 86400);
        if (days) {
            return days + ' day' + numberEnding(days);
        }
        var hours = Math.floor((temp %= 86400) / 3600);
        if (hours) {
            return hours + ' hour' + numberEnding(hours);
        }
        var minutes = Math.floor((temp %= 3600) / 60);
        if (minutes) {
            return minutes + ' minute' + numberEnding(minutes);
        }
        var seconds = temp % 60;
        if (seconds) {
            return seconds + ' second' + numberEnding(seconds);
        }
        return 'less than a second'; //'just now' //or other string you like;
    }
    
    
    render() {
        return <View>
        <Text>2018-09-04 04:39:00</Text>
        <View style={{flexDirection: 'row'}}><Text style={{marginLeft: 10, marginTop: 10, fontSize: 25}}>Name: {this.props.data.name} - </Text><Text style={{marginTop: 10, fontSize: 25, color: (this.props.data.runtime.connected ? 'green' : 'black')}}>{this.props.data.runtime.connected ? 'online' : 'offline'}</Text></View>
        <Text style={{marginLeft: 10, marginTop: 10, fontSize: 20}}>Temp: {this.props.data.runtime.actualTemperature * .1} F</Text>
        <Text style={{marginLeft: 10, marginTop: 10, fontSize: 20}}>Desired Heat Temp: {this.props.data.runtime.desiredHeat * .1} F</Text>
        <Text style={{marginLeft: 10, marginTop: 10, fontSize: 20, marginBottom: 40}}>Humidity: {this.props.data.runtime.actualHumidity} %</Text>
         <Text style={{marginLeft: 10, marginTop: 10, fontSize: 20}}>HVAC Setting: {this.props.data.settings.hvacMode}</Text>
         <Text style={{marginLeft: 10, marginTop: 10, fontSize: 20}}>Vent Setting: {this.props.data.settings.vent}</Text>
        </View>;
    }
}

export default class HomeClimate extends React.Component {
    async setAccessToken(accessTokenResponse) {
        let accessToken = accessTokenResponse.access_token;
        
        console.log('accessToken: ' + accessToken);
        
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + accessToken);
        
        let response = await fetch('https://api.ecobee.com/1/thermostat?json={"selection":{"includeAlerts":"true","selectionType":"registered","selectionMatch":"","includeEvents":"true","includeSettings":"true","includeRuntime":"true"}}', {headers: headers});
        
        if (response['status'] == 401 && response['url'] != 'https://api.ecobee.com/') {
            console.log('Unauthorized from a different URL. Trying again with auth credentials at new target...');
            
            response = await fetch(response['url'], {headers: headers});
        }
        
        console.log(response);
        let responseJson = await response.json();
        
        console.log(safeJsonStringify(responseJson));
        
        this.setState({launchOAuth: false, data: responseJson});
    }
    
    launchOAuthPage() {
        this.setState({launchOAuth: true});
    }
    
    async componentDidMount() {
        accessTokens.get((accessToken) => {
          let headers = new Headers();
          headers.append('Cookie', 'expiringToken=' + accessToken);
        
          fetch("https://pennidinh.com/secure/getAccessToken.cgi?state=ECOBEE", {headers: headers})
            .then(res => res.json())
            .then(res => this.setAccessToken(res))
            .catch(err => this.launchOAuthPage());
        });
    }
    
    newThermostat(thermostat) {
        return <Thermostat data={thermostat}/>;
    }
    
    render() {
        if (this.state != null && this.state.launchOAuth === true)
        {
            return (<View style={{flex: 1}}><WebView    source={{uri: 'https://api.ecobee.com/authorize?client_id=590GHuEz5KU23XIzIgrV9eYMpabIAeGg&state=ECOBEE&response_type=code&redirect_uri=' + encodeURIComponent('https://pennidinh.com/unsecure/acceptAuthCode.cgi') + '&scope=smartWrite'}}
                    style={{width: '100%', height:'90%'}}/></View>);
        }
        else if (this.state != null && this.state.data != null)
        {
            let thermostats = this.state.data.thermostatList
                .map(thermostat => (this.newThermostat(thermostat)));
            return <ScrollView style={{paddingTop: 30}}>{thermostats}</ScrollView>;
        }
        else
        {
            return <WaitingSpinner />;
        }
    }
}
