const safeJsonStringify = require('safe-json-stringify');
const uuidv1 = require('uuid/v1');
var AsyncLock = require('async-lock');
const serverEndpoint = require('./ServerEndpoint');
var lock = new AsyncLock();
const appSecretKeyClientSideDataStoreKey = '182d9921d-7498-4a00-9740-50c12cdcf992sdfdss';
const appAccessTokenDataStoreKey = '1c3eff6d3-a1e6-4bb9-86a9-4451f3d26a7fsdfdss';
const appAccessTokenExpirationTimeDataStoreKey = '1b50b1c15-0cc5-4c44-b686-d1569379034esdfds';
import * as SecureStore from 'expo-secure-store';
import * as WebBrowser from 'expo-web-browser';
import * as Linking from 'expo-linking';

const fetchAccessToken = function(endpoint, appSecretKey, proxyPort, resCallback, errCallback) {
    const headers = new Headers();
    headers.append('Cookie', 'pennidinhProxyAddress=' + proxyPort);
    fetch('https://' + endpoint + '/secure-node-js/getAccessToken.js?appSecretKey=' + appSecretKey, {headers: headers})
        .then(res => { const a = res.json(); console.log(a); return a; })
        .then(resCallback)
        .catch(errCallback);
}

const logUserIntoPenniDinh = function(provider, externalEndpoint, callback) {
    serverEndpoint.get(function(endpoint, proxyPort) {
        const redirectUrl = Linking.makeUrl();

        const callbackFun = function(browserClosedResult) {
            if (browserClosedResult.type === 'cancel' || browserClosedResult.type === 'dismiss') {
              console.log('User closed app browser manually indicating account linking was not complete (or the user is really quick!)');
              callback(null);
            } else if (browserClosedResult.type === 'success') {
              if (browserClosedResult.url.indexOf(redirectUrl) !== 0) {
                console.error('success from web browser but url returned was: ' + browserClosedResult.url + ' and expected: ' + redirectUrl);
                callback(null);
              }
              const parsed = Linking.parse(browserClosedResult.url);

              console.log('parsed url result: ' + safeJsonStringify(parsed));
              const queryParams = parsed['queryParams'];
              const appSecretKey = queryParams['appSecretKey'];

              SecureStore.setItemAsync(appSecretKeyClientSideDataStoreKey + provider + externalEndpoint, appSecretKey).then(function(result) {
                fetchAccessToken(endpoint, appSecretKey, proxyPort, (res) => {
                  if (res.accessToken) {
                    lock = new AsyncLock();

                    SecureStore.setItemAsync(appAccessTokenExpirationTimeDataStoreKey + provider + externalEndpoint, res.accessTokenExpirationEpochTime.toString())
                      .then(function() {
                        SecureStore.setItemAsync(appAccessTokenDataStoreKey + provider + externalEndpoint, res.accessToken)
                          .then(function() {
                            callback(res.accessToken);
                          });
                      });
                  }
                });
              }.bind(this));
            }
        }.bind(this);

        const openUrl = 'https://' + endpoint + '/secure-node-js/loginV2.js?isAppLogin=true&provider=' + provider + '&hostname=' + encodeURIComponent(endpoint) + '&redirectTo=' + encodeURIComponent(redirectUrl);
        if (proxyPort) {
          WebBrowser.openAuthSessionAsync('https://' + endpoint + '/cgi-bin/setProxyEndpoint.cgi?pennidinhProxyPort=' + proxyPort + '&redirectTo=' + encodeURIComponent(openUrl)).then(callbackFun);
        } else {
          WebBrowser.openAuthSessionAsync(openUrl).then(callbackFun);
        }
    });
};

const getNewAccessToken = function(callback, hubUnavailableCallback, ignoreLock, provider) {
    const externalEndpoint = serverEndpoint.getExternalEndpoint();

    if (ignoreLock) {
        lock = new AsyncLock();
    }

    console.log('Attempting to acquire access token lock...');
    lock.acquire('abc', function(done) {
       console.log('Access Token lock acquired');
       SecureStore.getItemAsync(appSecretKeyClientSideDataStoreKey + provider + externalEndpoint).then(function(appSecretKey) {
         serverEndpoint.get(function(endpoint, proxyPort) {
             if (!endpoint) {
                 console.log('PenniDinh hub unavailable');
                 lock = new AsyncLock();
                 done(null, null);
                 if (hubUnavailableCallback) {
                     hubUnavailableCallback();
                 }
                 return;
             }

             const launchLoginWindow = () => {
                 console.log('Launching window to log user in to PenniDinh');

                 if (!provider) {
                     done(null, null);
                     lock = new AsyncLock();
                     callback(null);
                     throw new Error("expected provider to be present!");
                 }

                 logUserIntoPenniDinh(provider, externalEndpoint, function (accessToken) {
                     done(null, null);
                     lock = new AsyncLock();
                     callback(accessToken);
                 });
             }

             if (!appSecretKey) {
                 launchLoginWindow();
             } else {
                 fetchAccessToken(endpoint, appSecretKey, proxyPort, (res) => {
                     SecureStore.setItemAsync(appAccessTokenExpirationTimeDataStoreKey + provider + externalEndpoint, res.accessTokenExpirationEpochTime.toString());
                     SecureStore.setItemAsync(appAccessTokenDataStoreKey + provider + externalEndpoint, res.accessToken);

                     lock = new AsyncLock();
                     done(null, null);

                     callback(res.accessToken);
                 }, function(err) {
                     console.log(err);

                     launchLoginWindow();
                 });
             }
          });
     }.bind(this));
   }, function(err, ret) {
                 if (err) {
                 console.log(err);
                     return;
                 }
                 });
};

module.exports.switchHubs = (callback) => {

    if (callback) {
        callback();
    }
}

module.exports.logout = (callback) => {

    const externalEndpoint = serverEndpoint.getExternalEndpoint();
    SecureStore.getItemAsync(externalEndpoint + '-provider').then(function (provider) {
        if (!provider) {
            throw new Error('Provider must be set!');
        }

        SecureStore.deleteItemAsync(appAccessTokenExpirationTimeDataStoreKey + provider + externalEndpoint)
            .then(function() {
                SecureStore.deleteItemAsync(appAccessTokenDataStoreKey + provider + externalEndpoint).then(function() {
                    SecureStore.deleteItemAsync(appSecretKeyClientSideDataStoreKey + provider + externalEndpoint).then(function() {
                        if (callback) { callback(); }
                    });
                });
            });
    }.bind(this));
};

currentProviderListenerCallbacks = {};
module.exports.registerCurrentProviderListener = (key, callback) => {
    currentProviderListenerCallbacks[key] = callback;
};
const currentProviderUpdated = function(provider) {
    for (var key in currentProviderListenerCallbacks) {
        currentProviderListenerCallbacks[key](provider);
    }
};

module.exports.setProvider = (callback, provider, thisExternalEndpoint) => {
    currentProviderUpdated(provider);
    SecureStore.setItemAsync(thisExternalEndpoint + '-provider', provider).then(callback);
};

module.exports.get = (callback, hubUnavailableCallback, ignoreLock) => {
    const handle = function (accessTokenExpirationEpochTime, accessToken, provider) {
        console.log('Retrieving PenniDinh login data data...');

        console.log('Retrieved accessTokenExpirationEpochTime: ' + accessTokenExpirationEpochTime);
        console.log('Retrieved accessToken: ' + accessToken);
        console.log('Retrieved provider: ' + provider);

        if (!accessTokenExpirationEpochTime || ((parseInt(accessTokenExpirationEpochTime) - (10 * 60)) < (new Date().getTime() / 1000))) {
            getNewAccessToken(callback, hubUnavailableCallback, ignoreLock, provider);
        } else {
            callback(accessToken)
        }
    }

    const externalEndpoint = serverEndpoint.getExternalEndpoint();
    SecureStore.getItemAsync(externalEndpoint + '-provider').then(function (provider) {
        if (!provider) {
            throw new Error('Provider must be set!');
        }
        SecureStore.getItemAsync(appAccessTokenExpirationTimeDataStoreKey + provider + externalEndpoint).then(function(accessTokenExpirationEpochTime) {
            SecureStore.getItemAsync(appAccessTokenDataStoreKey + provider + externalEndpoint).then(function (accessToken) {
                handle(accessTokenExpirationEpochTime, accessToken, provider); }); });
    }.bind(this));
};
